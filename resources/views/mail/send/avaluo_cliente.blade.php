@component('mail::message')
# Sr/a {{ $nombre }}

{{ $observacion }}

Gracias,<br>
{{ config('app.name') }}
@endcomponent
