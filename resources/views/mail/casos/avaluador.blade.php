@component('mail::message')
# Buenos días.

Se envían orden de avalúo y documentos correspondientes al predio a visitar.

<strong>Destino Avalúo:</strong> {{ $entidad['nombre'] }}<br>
<strong>Cliente:</strong> {{ $cliente['first_name'] }} {{ $cliente['last_name'] }}<br>
<strong>Dirección:</strong> {{ $direccion }}<br>

<strong>Observación:</strong> {{ $observacion }} <br><br>

<strong>Fecha visita:</strong> {{ Jenssegers\Date\Date::parse($caso->fecha_visita)->format('l j') }} de {{ Jenssegers\Date\Date::parse($caso->fecha_visita)->format('F') }}  ({{ $caso->hora_visita }})<br><br>
<strong>Fecha de entrega:</strong> {{ Jenssegers\Date\Date::parse($caso->fecha_entrega_informe)->format('l j') }} de {{ Jenssegers\Date\Date::parse($caso->fecha_entrega_informe)->format('F') }}<br><br>

<strong>Información contacto</strong>
<strong>Nombre:</strong> {{ $contacto }}<br>
<strong>Teléfono:</strong> {{ $telefono_contacto }}<br>
<strong>Correo:</strong> {{ $correo }}<br>
<br>
@foreach ($attach as $adjunto)

@if($adjunto->file_certificacion != "null")
<a href="http://moravain.online/{{$adjunto->file_certificacion}}">
	{{$adjunto->file_certificacion}}</a>
<br>
@endif

@if($adjunto->file_identificacion != null)
<a href="http://moravain.online/{{$adjunto->file_identificacion}}">
	{{$adjunto->file_identificacion}}</a>
<br>
@endif

@if($adjunto->file_soporte != null)
<a href="http://moravain.online/{{$adjunto->file_soporte}}">
	{{$adjunto->file_soporte}}</a>
<br>
@endif

@if($adjunto->file_escritura != null)
<a href="http://moravain.online/{{$adjunto->file_escritura}}">
	{{$adjunto->file_escritura}}</a>
<br>
@endif

@endforeach



@component('mail::button', ['url' => 'http://moravain.online/login'])
Ir al sistema
@endcomponent

Cualquier inquietud con gusto será atendida.<br>

Cordialmente;<br>
{{ config('app.name') }}
@endcomponent
{{-- 5T4c{R!GD]1N --}}
{{-- SHA256:EuVXzpEz2pV3LHbTckUtF4S4uoUcKDO7UREskL+AzRg  --}}

