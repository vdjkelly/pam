@isset($user)
{!! Form::model($user, ['route' => ['usuarios.update', $user->id], 'class' => 'form-horizontal']) !!}
{{ method_field('PATCH') }}
@endisset

@empty($user)
    {!! Form::open(['route' => 'usuarios.store', 'class' => 'form-horizontal']) !!}
@endempty
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Nombre</label>
                <div class="controls">
                    <input type="text" name="name" class="form-control  @error('name') is-invalid @enderror" data-validation-required-message="Este campo es requerido" placeholder="Jesus" value="@isset($user){{ $user->name }}@endisset @empty($user){{ old('name') }}@endempty">
                    @error('name')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label>Apellido</label>
                <div class="controls">
                    <input type="text" name="last_name" class="form-control  @error('last_name') is-invalid @enderror" data-validation-required-message="Este campo es requerido" placeholder="Sanchez" value="@isset($user){{ $user->last_name }}@endisset @empty($user){{ old('last_name') }}@endempty">
                    @error('last_name')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label>Email</label>
                <div class="controls">
                    <input type="email" name="email" class="form-control  @error('email') is-invalid @enderror" data-validation-required-message="Este campo es requerido" placeholder="jjsanchez@gmail.com" value="@isset($user){{ $user->email }}@endisset @empty($user){{ old('email') }}@endempty">
                    @error('email')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label>Contraseña</label>
                <div class="controls">
                    <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" @empty($user) data-validation-required-message="Este campo es requerido"@endempty placeholder="**********">
                    @error('password')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label>Confirmar contraseña</label>
                <div class="controls">
                    <input type="password" name="password_confirmation" class="form-control" @empty($user)data-validation-required-message="Este campo es requerido"@endempty  placeholder="**********">
                </div>
            </div>
            <div class="divider divider-danger">
                <div class="divider-text">Selecciona el role</div>
            </div>
            <div class="form-group">
                    {!! Form::select('roles[]', $roles->pluck('name', 'id'), isset($user) ? $user->roles->pluck('id')->toArray() : null, array('class' => 'form-control')) !!}
                    @error('roles[]')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
{!! Form::close() !!}