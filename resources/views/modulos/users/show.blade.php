@extends('layouts.app')



@section('content')

    <div class="content-body">
        <section class="input-validation">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Editar cuenta</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form action="{{ route('getUpdateInfo') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <div class="controls">
                                            <input type="text" name="name" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre" value="{{ auth()->user()->name }}">
                                            @error('name')
                                            <span class="text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Apellido</label>
                                        <div class="controls">
                                            <input type="text" name="last_name" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Apellido" value="{{ auth()->user()->last_name }}">
                                            @error('last_name')
                                            <span class="text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <fieldset class="form-group">
                                        <label for="basicInputFile">Imagen</label>
                                        <div class="custom-file">
                                            <input type="file" name="avatar" class="custom-file-input" id="inputGroupFile01">
                                            <label class="custom-file-label" for="inputGroupFile01">Buscar imagen</label>
                                        </div>
                                        @error('avatar')
                                        <span class="text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                        @enderror
                                    </fieldset>

                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('scripts')

@endpush
