@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <style>
        html body .content.app-content {
            overflow: auto;
        }
    </style>
@endpush

@section('content')
<div class="row">
    <div class="col-xl-12">
        

        <div class="col-md-12 col-sm-12">
            <div class="card overflow-auto">
                <div class="card-header">
                    <h4 class="card-title">Realizar observacion</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form method="POST" action="{{ route('observaciones.store') }}" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">
                            <div class="form-group">
                                <textarea class="form-control" id="observacion" rows="2" name="observacion" placeholder="Observaciones avaluo"></textarea>
                            </div>
                           <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                            
                            <div class="col-lg-6 col-md-12">
                                <fieldset class="form-group">
                                    <label for="basicInputFile">Subir archivo</label>
                                    <div class="custom-file">
                                        <input type="file" name="observacion_file[]" class="custom-file-input" id="customFileLang"
                                        lang="es" multiple>
                                        <label class="custom-file-label" for="inputGroupFile01">Seleccionar Archivo de
                                            Identificación</label>
                                    </div>
                                </fieldset>
                            </div>
                           </div>
                        </form>
                        @error('observacion')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card overflow-auto" style="height: 517.683px;">
            <div class="card-header">
                <h4 class="card-title">Listado de observaciones</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                        <div class="list-group">
                                @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                                    <div class="list-group-item list-group-item-action">
                                        <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                        <small class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                        </div>
                                        <p class="mb-1">{{ $observacion->observacion }}</p>
                                        {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}
                                        @if ($observacion->observacion_file)
                                            @foreach ($observacion->observacion_file as $file)
                                                <p class="mb-1"><a target="__blank" href="{{ asset($file->observacion_file) }}">{{$file->observacion_file}}</a></p>

                                            @endforeach
                                            
                                        @endif
                                    </div>
                                @empty

                                @endforelse
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
