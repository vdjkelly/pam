@extends('layouts.app')

@push('styles')

@endpush

@section('content')
<div class="row justify-content-center">
        <span class="mb-2 mr-3 text-bold-700">Fecha solicitud: {{ Carbon\Carbon::parse($facturacion->avaluo->fecha_solicitud)->format('d/m/Y') }} </span>
        <span class="mb-2 mr-3 text-bold-700">Fecha entrega cliente {{ Carbon\Carbon::parse($facturacion->avaluo->fecha_entrega_cliente)->format('d/m/Y') }} </span>
        <span class="mb-2 mr-3 text-bold-700">Número avaluo: {{ $facturacion->avaluo->numero_avaluo }}</span>
</div>

<div class="row justify-content-center">

    @if ($facturacion->avaluo->avaluador_id != null)
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">

                    <h4 class="card-title">CLIENTE CURRENTE</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                       {{--  <a class="btn btn-primary p-2" href="{{ route('facturaciones.create', ['id' =>  $facturacion->avaluo->id]) }}">Enviar Avaluo </a> --}}

                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                         <div class="mt-1">
                            <h6 class="mb-0">Número avalúo: {{ $facturacion->avaluo->numero_avaluo }}</h6>
                         </div>
                         <div class="mt-1">
                            <h6 class="mb-0">
                                Consecutivo Entidades: {{ $facturacion->avaluo->avaluo_solicitud->consecutivo_entidades }} </h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">
                                Tipo Avaluo: {{ $facturacion->avaluo->tipo_avaluo->nombre }} </h6>
                        </div>
                         <div class="mt-1">
                            <h6 class="mb-0">Entidad: {{ $facturacion->avaluo->entidad->nombre }}</h6>
                         </div>

                         <div class="mt-1">
                            <h6 class="mb-0">Consecutivo entidades: {{ $facturacion->avaluo->avaluo_solicitud->consecutivo_entidades }}</h6>
                         </div>

                         <div class="mt-1">
                            <h6 class="mb-0">Contacto: {{ $facturacion->avaluo->avaluo_solicitud->contacto }}</h6>
                         </div>

                         <div class="mt-1">
                            <h6 class="mb-0">Correo: {{ $facturacion->avaluo->avaluo_solicitud->correo }}</h6>
                         </div>

                         <div class="mt-1">
                            <h6 class="mb-0">Nombre: {{ $facturacion->avaluo->avaluo_solicitud->nombre }}</h6>
                         </div>

                         <div class="mt-1">
                            <h6 class="mb-0">Certificaci&oacute;n:</h6>
                         </div>
                        @if ($facturacion->avaluo->avaluo_solicitud)
                            @foreach ($facturacion->avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                <p><a target="__blank" href="{{ asset($adj->file_certificacion) }}">{{$adj->file_certificacion}}</a></p>
                            @endforeach
                        @else
                            No hay archivos
                        @endif


                        <div class="mt-1">
                            <h6 class="mb-0">Escritura:</h6>
                         </div>
                        @if ($facturacion->avaluo->avaluo_solicitud)
                            @foreach ($facturacion->avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                <p><a target="__blank" href="{{ asset($adj->file_escritura) }}">{{$adj->file_escritura}}</a></p>
                            @endforeach
                        @else
                            No hay archivos
                        @endif


                        <div class="mt-1">
                            <h6 class="mb-0">Identificación:</h6>
                         </div>
                        @if ($facturacion->avaluo->avaluo_solicitud)
                            @foreach ($facturacion->avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                <p><a target="__blank" href="{{ asset($adj->file_identificacion) }}">{{$adj->file_identificacion}}</a></p>
                            @endforeach
                        @else
                            No hay archivos
                        @endif

                         

                         <div class="mt-1">
                            <h6 class="mb-0">Teléfono: {{ $facturacion->avaluo->avaluo_solicitud->telefono }}</h6>
                         </div>
                         <div class="mt-1">
                            <h6 class="mb-0">Celular: {{ $facturacion->avaluo->avaluo_solicitud->celular }}</h6>
                         </div>

                         @if ($facturacion->avaluo->facturado == false)
                        <form class="mb-5 mt-5" action="{{ route('facturaciones.store') }}" method="post">
                            @csrf
                        <input type="hidden" name="avaluo_id" value="{{ $facturacion->avaluo->id }}">
                        <input type="hidden" name="facturado" value="1">
                                <button type="submit" class="btn btn-primary">Terminar factura</button>
                            </form>
                            @else 
                            <button type="button" class="btn btn-primary">Proceso facturado</button>
                         @endif
                    </div>
                </div>

            </div>
        </div>
    @else
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">

                    <h4 class="card-title">CLIENTE NUEVO</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">

                    </div>
                </div>

            </div>
        </div>
    @endif

</div>
@endsection

@push('scripts')

@endpush
