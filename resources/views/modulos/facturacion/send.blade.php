@extends('layouts.app')

@push('styles')

@endpush

@section('content')
<div class="row">
        <div class="col-md-6 col-sm-6">
            <h4>Fecha de solicitud:  {{ $avaluo->created_at->format('Y-m-d H:i:s A') }}</h4>
        </div>

        <div class="col-md-6 col-sm-6">
                <h4>Fecha entrega cliente:  {{ $avaluo->fecha_entrega_cliente }}</h4>
            </div>
    </div>

<div class="row">
    <div class="col-md-4 col-sm-12">

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">INFORMACIÓN AVALUO </h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">

                        <div class="mt-1">
                            <h6 class="mb-0">Fecha para revisar: {{ \Carbon\Carbon::parse($avaluo->fecha_entrega_cliente)->format('Y-m-d') }} </h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">CLIENTE: {{ $avaluo->cliente->first_name }} {{ $avaluo->cliente->last_name }}</h6>
                        </div>
                    <div class="mt-1">
                        <h6 class="mb-0">
                                @isset($avaluo->cliente->nit)
                                   Nit {{ $avaluo->cliente->nit }}
                                @endisset

                                @isset($avaluo->cliente->identificacion)
                                Identificaci&oacute;n: {{ $avaluo->cliente->identificacion }}
                                @endisset
                        </h6>
                    </div>

                    <div class="mt-1">
                        <h6 class="mb-0">Teléfono: {{ $avaluo->cliente->telefono }}</h6>
                    </div>

                    <div class="mt-1">
                        <h6 class="mb-0">Celular: {{ $avaluo->cliente->celular }}</h6>
                    </div>

                    <div class="mt-1">
                        <h6 class="mb-0">Correo: {{ $avaluo->cliente->email }}</h6>
                    </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Tipo predio: {{ $avaluo->type_predio->nombre }}</h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Entidad: {{ $avaluo->entidad->nombre }}</h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Soporte ID:
                                @if ($avaluo->avaluo_solicitud)
                                    @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                        <p><a target="__blank" href="{{ asset($adj->file_identificacion) }}">{{$adj->file_identificacion}}</a></p>

                                    @endforeach
                                @else
                                    No hay archivos
                                @endif
                            </h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">Soporte CTL:
                                @if ($avaluo->avaluo_solicitud)
                                    @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                        <p><a target="__blank" href="{{ asset($adj->file_certificacion) }}">{{$adj->file_certificacion}}</a></p>

                                    @endforeach
                                @else
                                    No hay archivos
                                @endif
                            </h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Soporte escritura:
                                @if ($avaluo->avaluo_solicitud)
                                    @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                        <p><a target="__blank" href="{{ asset($adj->file_escritura) }}">{{$adj->file_escritura}}</a></p>
                                    @endforeach
                                @else
                                    No hay archivos
                                @endif
                            </h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Avaluador: {{ $avaluo->avaluador->name }} {{ $avaluo->avaluador->last_name }}</h6>
                        </div>
                    <div class="mt-1">
                        <h6 class="mb-0">Valor comercial: {{ $avaluo->valor_comercial }}</h6>
                    </div>


                        <div class="mt-1 mb-2">
                            <h6 class="mb-0">Descarga archivos adjuntos:
                                @if ($avaluo->file_avaluo)
                                    @foreach ($avaluo->file_avaluo as $adj)
                                        <p><a target="__blank" href="{{ asset($adj->informe) }}">{{$adj->informe}}</a></p>
                                    @endforeach
                                @else
                                    No hay archivos
                                @endif
                            </h6>
                        </div>
                </div>
            </div>
        </div>

        <div class="card">
        <div class="card-header">

            <h4 class="card-title">CLIENTE CURRENTE</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
                {{--  <a class="btn btn-primary p-2" href="{{ route('facturaciones.create', ['id' =>  $facturacion->avaluo->id]) }}">Enviar Avaluo </a> --}}

            </div>
        </div>
        <div class="card-content collapse show">
            <div class="card-body">
                <div class="mt-1">
                    <h6 class="mb-0">Número avalúo: {{ $avaluo->numero_avaluo }}</h6>
                </div>

                <div class="mt-1">
                    <h6 class="mb-0">Entidad: {{ $avaluo->entidad->nombre }}</h6>
                </div>

                <div class="mt-1">
                    <h6 class="mb-0">Consecutivo entidades: {{ $avaluo->avaluo_solicitud->consecutivo_entidades }}</h6>
                </div>

                <div class="mt-1">
                    <h6 class="mb-0">Contacto: {{ $avaluo->avaluo_solicitud->contacto }}</h6>
                </div>

                <div class="mt-1">
                    <h6 class="mb-0">Correo: {{ $avaluo->avaluo_solicitud->correo }}</h6>
                </div>

                <div class="mt-1">
                    <h6 class="mb-0">Nombre: {{ $avaluo->avaluo_solicitud->nombre }}</h6>
                </div>

                @if ($avaluo->avaluo_solicitud)
                    @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                        <p><a target="__blank" href="{{ asset($adj->file_certificacion) }}">{{$adj->file_certificacion}}</a></p>
                        <p><a target="__blank" href="{{ asset($adj->file_identificacion) }}">{{$adj->file_identificacion}}</a></p>
                    @endforeach
                @else
                    No hay archivos
                @endif

                <div class="mt-1">
                    <h6 class="mb-0">Identificación: {{ $avaluo->avaluo_solicitud->identificacion }}</h6>
                </div>

                <div class="mt-1">
                    <h6 class="mb-0">Teléfono: {{ $avaluo->avaluo_solicitud->telefono }}</h6>
                </div>
                <div class="mt-1">
                    <h6 class="mb-0">Celular: {{ $avaluo->avaluo_solicitud->celular }}</h6>
                </div>
            </div>
        </div>

    </div>


    </div>
    <div class="col-md-4 col-sm-4">
            <div class="card ">
                    <div class="card-header">
                        <h4 class="card-title">ENVIAR AVALUO</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                                <form method="POST" action="{{ route('avaluos.send.store') }}" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">

                                <div class="form-group">
                                        <label>Nombre</label>
                                        <div class="controls">
                                            <input type="text" name="nombre" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre" value="{{ old('nombre') }}">
                                            @error('nombre')
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>






                                        <div class="form-group">
                                                <label>Email</label>
                                                <div class="controls">
                                                    <input type="text" name="email" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Email" value="{{ old('email') }}">
                                                    @error('email')
                                                        <span class="text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>

                                        <div class="form-group">
                                                <label>Número avalúo</label>
                                                <div class="controls">
                                                    <input type="text" name="numero_avaluo" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre" value="{{ $avaluo->numero_avaluo }}" disabled>
                                                    @error('numero_avaluo')
                                                        <span class="text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>


                                    <div class="form-group">
                                        <label>Dirección</label>
                                        <div class="controls">
                                            <input type="text" name="direccion" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre" value="{{ $avaluo->avaluo_solicitud->direccion  }}" disabled>
                                            @error('numero_avaluo')
                                            <span class="text-danger" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                            <fieldset class="form-group">
                                                    <div class="custom-file">
                                                            <input type="file" name="informe" class="custom-file-input" id="customFileLang" lang="es">
                                                            <label class="custom-file-label" for="customFileLang">Informe avaluo</label>
                                                    </div>
                                            </fieldset>

                                    <div class="form-group">
                                        <textarea class="form-control" id="observacion" rows="2" name="observacion"     placeholder="Observaciones avaluo" required></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>

    <div class="col-md-4 col-sm-12">
        <div class="card">

            <div class="card-content collapse show">
                <div class="card-body overflow-scroll" style="height: 580px">
                    <h4 class="card-title mt-5">Observaciones del caso</h4>
                    <div class="list-group">

                        @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                            <a href="#" class="list-group-item list-group-item-action">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                    <small class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                </div>
                                <p class="mb-1">{{ $observacion->observacion }}</p>
                                {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}

                            </a>
                        @empty

                        @endforelse

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
