@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/data-list-view.css') }}">
@endpush

@section('content')
    <div class="content-body">
        <!-- Data list view starts -->
        <section id="data-list-view" class="data-list-view-header">

            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table data-list-view">
                        <thead>
                                <tr>
                                    <th>Numero avaluo</th>
                                    <th>Nombre y apellido</th>
                                    <th>Telefono</th>
                                    <th>Celular</th>
                                    <th>Correo</th>
                                    <th>Nombre factura</th>
                                    <th>Identificación</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($avaluos as $row)
                                <tr>
                                    <td> {{ $row->numero_avaluo }} </td>
                                    <td>{{ $row->cliente->first_name }}  {{ $row->cliente->lastname }}</td>
                                    <td>{{ $row->cliente->telefono }}</td>
                                    <td>{{ $row->cliente->celular }}</td>
                                    <td>{{ $row->cliente->email }}</td>
                                    <td>{{ $row->cliente->facturacion[0]['factura'] }}</td>
                                    <td>
                                        @isset($row->cliente->nit)
                                           {{ $row->cliente->nit }}
                                        @endisset

                                        @isset($row->cliente->identificacion)
                                            {{ $row->cliente->identificacion }}
                                        @endisset
                                    </td>



                                    @if ($row->facturado == false)
                                        <td><a role="button" class="btn btn-outline-success waves-effect waves-light text-right" href="{{ route('facturaciones.show', ['id' => $row->facturacion_id]) }}">Ver facturación</a></td>
                                        @else
                                        <td><a role="button" class="btn btn-outline-success waves-effect waves-light text-right" href="#">Facturado</a></td>
                                    @endif
                                </tr>
                                @empty

                                @endforelse

                            </tbody>
                </table>
            </div>
            <!-- DataTable ends -->
        </section>
        <!-- Data list view end -->

    </div>
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/ui/data-list-view.js') }}"></script>

    <script>


    </script>
@endpush
