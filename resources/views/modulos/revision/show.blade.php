@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <style>
        html body .content.app-content {
            overflow: auto;
        }
    </style>
@endpush

@section('content')
<div class="row">
        <div class="col-md-6 col-sm-6">
            <h4>Fecha de solicitud:  {{ $avaluo->created_at->format('Y-m-d H:i:s A') }}</h4>
        </div>

        <div class="col-md-6 col-sm-6">
                <h4>Numero avaluo:  {{ $avaluo->numero_avaluo }}</h4>
            </div>
    </div>

<div class="row">

        <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">INFORMACIÓN </h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">

                            <h4 class="mt-5">Fecha de entrega avaluador:  {{ Carbon\Carbon::parse($avaluo->fecha_entrega_informe)->format('Y-m-d') }}</h4>
                            <div class="mt-1">
                            <h6 class="mb-0">Nombre y Apellido: {{ $avaluo->cliente->first_name }} {{ $avaluo->cliente->last_name }}</h6>
                            </div>

                            <div class="mt-1">
                                    <h6 class="mb-0">Tipo predio: @isset($avaluo->type_predio)
                                        {{ $avaluo->type_predio->nombre }}
                                    @endisset</h6>
                                </div>


                                <div class="mt-1">
                                        <h6 class="mb-0">Avaluador: @isset($avaluo->avaluador->name) {{ $avaluo->avaluador->name }} @endisset @isset($avaluo->avaluador->last_name) {{ $avaluo->avaluador->last_name }} @endisset

                                    </h6>
                                    </div>

                            <div class="mt-1">
                                <h6 class="mb-0">valor comercial:  {{ $avaluo->valor_comercial }} </h6>
                            </div>
                            <div class="mt-1">
                                <h6 class="mb-0">Ciudad: @isset($avaluo->city)
                                    {{ $avaluo->city->nombre }}
                                @endisset</h6>
                            </div>





                            <h4 class="card-title">ASIGNAR REVISOR </h4>

                            <form method="POST" action="{{ route('revisor.store') }}" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">
                                <div class="form-group">
                                    <label>REVISOR TÉCNICO</label>
                                    <div class="controls">
                                        {{ Form::select('revisor_tecnico_id', $revisores->pluck('name', 'id'), null, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'revisor_tecnico_id']) }}
                                    </div>
                                </div>

                                <div class="controls">
                                    <label>
                                        Fecha de envio
                                    </label>
                                    <div class="form-group">
                                        <input type='text' name="fecha_entrega_cliente" class="form-control format-picker-fecha_entrega_cliente"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" id="observacion" rows="2" name="observacion" placeholder="Observaciones avaluo"></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </form>

                        </div>
                    </div>
                </div>
        </div>

        <div class="col-md-6 col-sm-12">
                <div class="card overflow-auto" style="height: 517.683px;">
                    <div class="card-header">
                        <h4 class="card-title">Observaciones del caso</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="list-group">

                                        @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                                <small class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                                </div>
                                                <p class="mb-1">{{ $observacion->observacion }}</p>
                                                {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}

                                            </a>
                                        @empty

                                        @endforelse

                                    </div>
                        </div>
                    </div>
                </div>
            </div>


</div>

</div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

<script>
$(document).ready(function(){
        $(".visita").click(function(evento){

            var valor = $(this).val();

            if(valor == 0){
                $("#content").css("display", "none");
            }else{
                $("#content").css("display", "block");
            }
    });

    $('.format-picker-fecha_entrega_cliente').datepicker({
        format: 'yyyy-mm-dd'
    });
});
</script>
@endpush
