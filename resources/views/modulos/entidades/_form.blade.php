@isset($entidad)
{!! Form::model($entidad, ['route' => ['entidades.update', $entidad->id], 'class' => 'form-horizontal']) !!}
{{ method_field('PATCH') }}
@endisset

@empty($entidad)
    {!! Form::open(['route' => 'entidades.store', 'class' => 'form-horizontal']) !!}
@endempty
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Nombre Entidad</label>
                <div class="controls">
                    <input type="text" name="nombre" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre" value="@isset($entidad){{ $entidad->nombre }}@endisset  @empty($entidad){{ old('nombre') }}@endempty">
                    @error('nombre')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label>Nombre usuario</label>
                <div class="controls">
                    <input type="text" name="username" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre usuario" value="@isset($entidad){{ $entidad->user->username }}@endisset  @empty($entidad){{ old('username') }}@endempty">
                    @error('username')
                    <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label>Email</label>
                <div class="controls">
                    <input type="email" name="email" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Apellido usuario" value="@isset($entidad){{ $entidad->user->email }}@endisset  @empty($entidad){{ old('email') }}@endempty">
                    @error('email')
                    <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <label>Teléfono</label>
                <div class="controls">
                    <input type="text" name="telefono" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Teléfono" value="@isset($entidad){{ $entidad->user->telefono }}@endisset  @empty($entidad){{ old('telefono') }}@endempty">
                    @error('telefono')
                    <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label>Celular</label>
                <div class="controls">
                    <input type="text" name="celular" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Celular" value="@isset($entidad){{ $entidad->user->celular }}@endisset  @empty($entidad){{ old('celular') }}@endempty">
                    @error('celular')
                    <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label>Nit</label>
                <div class="controls">
                    <input type="text" name="nit" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nit" value="@isset($entidad){{ $entidad->user->nit }}@endisset  @empty($entidad){{ old('nit') }}@endempty">
                    @error('nit')
                    <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label>Contraseña</label>
                <div class="controls">
                    <input type="password" name="password" class="form-control  @error('password') is-invalid @enderror" @empty($entidad) data-validation-required-message="Este campo es requerido"@endempty placeholder="**********">
                    @error('password')
                    <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label>Confirmar contraseña</label>
                <div class="controls">
                    <input type="password" name="password_confirmation" class="form-control" @empty($entidad)data-validation-required-message="Este campo es requerido"@endempty  placeholder="**********">
                </div>
            </div>

        </div>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
{!! Form::close() !!}
