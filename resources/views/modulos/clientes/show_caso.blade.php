@extends('layouts.app')

@section('content')
    <div class="content-body">
        <section class="input-validation">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Cancelar avaluo</h4>
<h4 class="text-danger">Tome en cuenta que al cancelarlo no hay vuelta atrás</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form action="{{ route('casos-cliente.update', ['id' => $avaluo->id]) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label>Observacion</label>
                                        <div class="controls">
                                        <textarea class="form-control" id="observacion" rows="2" name="observacion"
                                                  placeholder="Observaciones avaluo"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Cancelar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')

@endpush
