@extends('layouts.app')


@section('content')

    <div class="content-body">
        <!-- Input Validation start -->
        <section class="input-validation">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Editar datos del cliente</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                            <form method="POST" action="{{ route('updateDatosCliente', ['id' => $avaluo->id]) }}" class="form-horizontal"
                    enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <input type="hidden" value="{{ $avaluo->cliente->id }}" name="cliente_id">
                    <div class="form-group">
                        <label>Nombre</label>
                        <div class="controls">
                            <input type="text" name="first_name" class="form-control"
                                data-validation-required-message="Este campo es requerido"
                                placeholder="Nombre"
                                value="{{ $avaluo->cliente->first_name }}">
                        </div>
                    </div>

                     <div class="form-group">
                        <label>Apellido</label>
                        <div class="controls">
                            <input type="text" name="last_name" class="form-control"
                                data-validation-required-message="Este campo es requerido"
                                placeholder="Apellido"
                                value="{{ $avaluo->cliente->last_name }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Correo</label>
                        <div class="controls">
                            <input type="email" name="email" class="form-control"
                                data-validation-required-message="Este campo es requerido"
                                placeholder="Correo"
                                value="{{ $avaluo->cliente->email }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="controls">
                            <label>
                                Departamento
                            </label>
                            <div class="form-group">
                                {{ Form::select('departament_id', $departamentos->pluck('nombre', 'id'), $avaluo->departament_id, ['placeholder' => 'Por favor seleccione', 'class' => 'select2-size-lg form-control', 'id' => 'departament_id', 'required' => 'required', 'data-validation-required-message' => 'Este campo es requerido']) }}
                                @error('departament_id')
                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Ciudad o Municipio</label>
                        <div class="controls">
                            <select class="form-control" name="city_id" id="city"></select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label>Teléfono</label>
                        <div class="controls">
                            <input type="text" name="telefono" class="form-control"
                                data-validation-required-message="Este campo es requerido"
                                placeholder="Teléfono"
                                value="{{ $avaluo->cliente->telefono }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Celular</label>
                        <div class="controls">
                            <input type="text" name="celular" class="form-control"
                                data-validation-required-message="Este campo es requerido"
                                placeholder="Celular"
                                value="{{ $avaluo->cliente->celular }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Direccion</label>
                        <textarea class="form-control" id="direccion" rows="3" name="direccion" placeholder="Direccion"
                                  data-validation-required-message="Este campo es requerido">{{  $avaluo->avaluo_solicitud->direccion }}</textarea>
                        @error('direccion')
                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Tipo predio</label>
                        <div class="controls">
                            {{ Form::select('type_predio_id', $tipo_predios->pluck('nombre', 'id'), $avaluo->type_predio_id, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'type_predio_id']) }}
                        </div>
                    </div>



                     <div class="form-group">
                        <label>Tipo cliente</label>
                        <div class="controls">
                            {{ Form::select('tipo_cliente_id', $tipo_clientes->pluck('name', 'id'), $avaluo->cliente->tipo_cliente_id, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'tipo_cliente_id']) }}
                            @error('tipo_cliente_id')
                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group identificacion " @if ($avaluo->cliente->tipo_cliente_id == 2)
                        style="display:none"
                    @endif>
                        <label>Identificación</label>
                        <div class="controls">
                            <input type="text" name="identificacion" class="form-control"
                                data-validation-required-message="Este campo es requerido"
                                placeholder="Identificacion"
                                value="{{ $avaluo->cliente->identificacion }}">
                        </div>
                    </div>

                    <div class="form-group nit" @if ($avaluo->cliente->tipo_cliente_id == 1)
                        style="display:none"
                    @endif>
                        <label>Nit</label>
                        <div class="controls">
                            <input type="text" name="nit" class="form-control" placeholder="Nit" value="{{ $avaluo->cliente->nit }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <div class="custom-file">
                                <input type="file" name="file_identificacion[]" class="custom-file-input" id="customFileLang"
                                       lang="es" multiple>
                                <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                                    Identificación</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Certificación Tradición</label>
                        <div class="controls">
                            <input type="text" name="certificacion" class="form-control" required
                                   data-validation-required-message="Este campo es requerido"
                                   placeholder="Certificación Tradición" value="{{ $avaluo->avaluo_solicitud->certificacion }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                            <div class="custom-file">
                                <input type="file" name="file_certificacion[]" class="custom-file-input" id="customFileLang"
                                       lang="es" multiple>
                                <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                                    Certificación</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Escritura publica (Ultima adquisición)</label>
                        <div class="controls">
                            <input type="text" name="escritura" class="form-control" required
                                   data-validation-required-message="Este campo es requerido"
                                   placeholder="Escritura publica (Ultima adquisición)" value="{{ $avaluo->avaluo_solicitud->escritura }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" name="file_escritura[]" class="custom-file-input" id="customFileLang" lang="es" multiple>
                            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de Escritura</label>
                        </div>
                    </div>




                    <div class="alert alert-primary mb-2" role="alert">
                        <strong>DATOS</strong> FACTURACIÓN.
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div id="content">
                                <div class="form-group">
                                    <label>Nombre factura</label>
                                    <div class="controls">
                                        <input type="text" id="factura" name="factura" class="form-control" placeholder="Nombre factura"
                                               value="{{ $avaluo->facturacion->factura }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Telefono</label>
                                    <div class="controls">
                                        <input type="text" id="telefono_factura" name="telefono_factura" class="form-control"
                                               placeholder="Telefono" value="{{ $avaluo->facturacion->telefono_factura }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Correo</label>
                                    <div class="controls">
                                        <input type="email" id="email_factura" name="email_factura" class="form-control"
                                               placeholder="Correo" value="{{ $avaluo->facturacion->email_factura }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="direccion_factura" rows="2" name="direccion_factura"
                                              placeholder="Direccion" >{{ $avaluo->facturacion->direccion }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Identificacion</label>
                                    <div class="controls">
                                        <input type="text" id="identificacion_factura" name="identificacion_factura"
                                               class="form-control" placeholder="Identificacion"
                                               value="{{ $avaluo->facturacion->identificacion_factura }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file" name="file_identificacion_factura" class="custom-file-input"
                                               id="customFileLang" lang="es">
                                        <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                                            Identificacion</label>
                                    </div>
                                </div>
                            </div>
                            <div id="itaud_content">
                                <div class="form-group">
                                    <label>Banco (Consigno)</label>
                                    <div class="controls">
                                        <input type="text" name="banco_consigno" class="form-control" placeholder="Banco (Consigno)"
                                               value="{{ $avaluo->facturacion->banco_consigno }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Valor anticipo</label>
                                    <div class="controls">
                                        <input type="text" name="valor_anticipo" class="form-control" placeholder="Valor anticipo"
                                               onkeyup="format(this)" onchange="format(this)" value="{{ $avaluo->facturacion->valor_anticipo }}">
                                        @error('valor_anticipo')
                                        <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Valor anticipo</label>
                                    <div class="controls">
                                        <div class="custom-file">
                                            <input type="file" name="file_anticipo" class="custom-file-input" id="customFileLang"
                                                   lang="es">
                                            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                                                Anticipo</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Input Validation end -->
    </div>

@endsection

@push('scripts')
    <script>
        $("#tipo_cliente_id").change(function (e) {
            let tipo_cliente_id = e.target.value
            console.log(tipo_cliente_id);
            let nit = document.querySelector('.nit');
            let identificacion = document.querySelector('.identificacion');
            if (tipo_cliente_id == 1) {
                nit.style.display = "none";
                identificacion.style.display = "block";
            } else {
                identificacion.style.display = "none";
                nit.style.display = "block";
            }
        });




            $(document).ready(function(){
                let departament_id = "{{ $avaluo->departament_id }}"
                let city_id = "{{ $avaluo->city_id }}"
                $("#city").empty();//To reset cities
                $("#city").append("<option>Por favor seleccione</option>");
                $.get('/cities/' + departament_id + '/show', function (response) {
                    $(response).each(function (i) {//to list cities
                        $("#city").append("<option id='" + response[i].id + "' value='" + response[i].id + "'>" + response[i].nombre + "</option>")

                        $("#city").val(city_id).attr('selected','selected');
                    });
                });
                //$("#city").val(city_id)

            });

            $("#departament_id").change(function (e) {
                let departament_id = e.target.value
                $("#city").empty();//To reset cities
                $("#city").append("<option>Por favor seleccione</option>");
                $.get('/cities/' + departament_id + '/show', function (response) {
                    $(response).each(function (i) {//to list cities
                        $("#city").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>")
                    });
                });
            });
            function format(input) {
                var num = input.value.replace(/\./g, '');
                if (!isNaN(num)) {
                    num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
                    num = num.split('').reverse().join('').replace(/^[\.]/, '');
                    input.value = num;
                } else {
                    alert('Solo se permiten numeros');
                    input.value = input.value.replace(/[^\d\.]*/g, '');
                }
            }
    </script>
@endpush
