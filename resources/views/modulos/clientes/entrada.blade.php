@extends('layouts.app')


@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
@endpush

@section('content')
    <!-- Modal -->
    {{-- <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Nuevo caso</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Selecciona</h5>
                    <p>Presiona alguna de las dos opciones presentes, si el cliente es nuevo o  solo necesitas registrar la informacion del solicitante</p>
                </div>
                <div class="modal-footer">
                    <a role="button" class="btn btn-outline-primary waves-effect waves-light text-left" href="{{ route('casos.create') }}">Nuevo</a>
                    <a role="button" class="btn btn-outline-success waves-effect waves-light text-right" href="{{ route('casos.getNotNewCreate') }}">Existente</a>
                </div>
            </div>
        </div>
    </div> --}}

    <!-- Zero configuration table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Clientes</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                            <div class="table-responsive">
                                <table class="table zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>Numero avaluo</th>
                                        <th>Creado</th>
                                        <th>Consecutivo Entidad</th>
                                        <th>Estado</th>
                                        <th>Informe Avaluo</th>
                                        <th>Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($avaluos as $row)
                                        <tr>
                                            <td> {{ $row->numero_avaluo }} </td>
                                            <td>{{ $row->created_at->format('Y-m-d H:i:s A') }}</td>
                                            <td>{{ $row->avaluo_solicitud->consecutivo_entidades }}</td>

                                                @if($row->status == 0)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#">En proceso</a></td>
                                                @elseif($row->status == 1)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador asignado para visita</a></td>
                                                @elseif($row->status == 2)
                                                        <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Visita realizada con exito</a></td>
                                                @elseif($row->status == 3)
                                                        <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador no realizo la visita</a></td>
                                                @elseif($row->status == 4)
                                                        <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador reasignado para visita</a></td>
                                                @elseif($row->status == 5)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Proceso exitoso para facturar</a></td>
                                                @elseif($row->status == 6)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Devuelta por revision</a></td>
                                                @elseif($row->status == 7)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Correcciones para revisión</a></td>
                                                @elseif($row->status == 8)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluo enviado al cliente</a></td>
                                                @elseif($row->status == 9)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> PQRS-ENTIDAD</a></td>
                                                @elseif($row->status == 10)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Por revision</a></td>
                                                @endif
                                            <td>
                                                @if ($row->informe)
                                                <a href="{{ asset('archivos/'.$row->informe) }}" target="__blank">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                                
                                                @endif
                                            </td>
                                            <td>
                                            @if($row->status == 5)
                                                <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="{{ route('casos-cliente.show', ['id' => $row->id]) }}">Cancelar</a>
                                            @endif

                                            </td>

                                        </tr>
                                    @empty

                                    @endforelse

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Numero avaluo</th>
                                        <th>Creado</th>
                                        <th>Consecutivo Entidad</th>
                                        <th>Estado</th>
                                        <th>Informe Avaluo</th>
                                        <th>Acción</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Zero configuration table -->
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

    <script>
        $('.zero-configuration').DataTable({
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados puedes crear un <a role='button' class='btn btn-outline-primary waves-effect waves-light text-left' href='{{ route('casos.create') }}'>Nuevo</a> cliente",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
        });
    </script>
@endpush
