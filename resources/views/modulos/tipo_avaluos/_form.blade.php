@isset($avaluo)
{!! Form::model($avaluo, ['route' => ['tipos_avaluos.update', $avaluo->id], 'class' => 'form-horizontal']) !!}
{{ method_field('PATCH') }}
@endisset

@empty($avaluo)
    {!! Form::open(['route' => 'tipos_avaluos.store', 'class' => 'form-horizontal']) !!}
@endempty
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Tipo Avaluo</label>
                <div class="controls">
                    <input type="text" name="nombre" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Hipotecario" value="@isset($avaluo){{ $avaluo->nombre }}@endisset">
                    @error('nombre')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
{!! Form::close() !!}