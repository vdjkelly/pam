@extends('layouts.app')

@push('styles')

@endpush

@section('content')
    <div class="row justify-content-center mb-3">
       <strong> Detalles del Avaluos</strong>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">

                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        {{--  <a class="btn btn-primary p-2" href="{{ route('facturaciones.create', ['id' =>  $facturacion->avaluo->id]) }}">Enviar Avaluo </a> --}}

                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <p class="font-weight-bold black"><strong>INFORMACI&Oacute;N DEL AVALUO</strong></p>
                                <div class="mt-1">
                                    <h6 class="mb-0">Número avalúo: {{ $avaluo->numero_avaluo }}</h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Consecutivo entidades: @isset($avaluo->avaluo_solicitud->consecutivo_entidades)
                                        {{ $avaluo->avaluo_solicitud->consecutivo_entidades }}
                                    @endisset</h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Entidad: @isset($avaluo->entidad->nombre)
                                        {{ $avaluo->entidad->nombre }}
                                    @endisset</h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Tipo Avaluo: @isset($avaluo->tipo_avaluo->nombre)
                                        {{ $avaluo->tipo_avaluo->nombre }}
                                    @endisset</h6>
                                </div>


                                <div class="mt-1">
                                    <h6 class="mb-0">Tipo predio: @isset($avaluo->type_predio->nombre)
                                        {{ $avaluo->type_predio->nombre }}
                                    @endisset</h6>
                                </div>


                                <div class="mt-1">
                                    <h6 class="mb-0">Ciudad o Municipio: @isset($avaluo->city->nombre)
                                        {{ $avaluo->city->nombre }}
                                    @endisset</h6>
                                </div>
        
                                <div class="mt-1">
                                    <h6 class="mb-0">Direcci&oacute;n: {{ $avaluo->avaluo_solicitud->direccion  }}</h6>
                                </div>
                                
        
                                <div class="mt-1">
                                    <h6 class="mb-0">Soporte escritura:
                                        @if ($avaluo->avaluo_solicitud)
                                            @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                                <p><a target="__blank" href="{{ asset($adj->file_escritura) }}">{{$adj->file_escritura}}</a></p>
                                            @endforeach
                                        @else
                                            No hay archivos
                                        @endif
                                    </h6>
                                </div>
        
                                <div class="mt-1">
                                    <h6 class="mb-0">Soporte CTL:
                                        @if ($avaluo->avaluo_solicitud)
                                            @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                                <p><a target="__blank" href="{{ asset($adj->file_certificacion) }}">{{$adj->file_certificacion}}</a></p>
        
                                            @endforeach
                                        @else
                                            No hay archivos
                                        @endif
                                    </h6>
                                </div>
                               


                                <p class="font-weight-bold mt-3 black">DATOS DE LA OPERACI&Oacute;N</p>

                                <div class="mt-1">
                                    <h6 class="mb-0">Fecha creaci&oacute;n: {{ $avaluo->created_at->format('d-m-Y H:i:s A') }}</h6>
                                </div>


                                <div class="mt-1">
                                    <h6 class="mb-0">Fecha/Prog/Visita:  
                                        @isset($avaluo->fecha_visita)
                                        {{  Carbon\Carbon::parse($avaluo->fecha_visita)->format("d-m-Y")  }} - {{ $avaluo->hora_visita }}
                                        @endisset
                                    </h6>
                                </div>

                                <div class="mt-3">
                                    <h6 class="mb-0">Avaluador: @isset($avaluo->avaluador->name)
                                        {{ $avaluo->avaluador->name }}
                                    @endisset  @isset($avaluo->avaluador->last_name)
                                    {{ $avaluo->avaluador->last_name }}
                                    @endisset</h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Entreg Prog/Avaluad:  
                                        @isset($avaluo->fecha_entrega_informe)
                                            {{ Carbon\Carbon::parse($avaluo->fecha_entrega_informe)->format("d-m-Y H:i:s A")  }}
                                        @endisset
                                    </h6>
                                </div>

                                
                                <div class="mt-1">
                                    <h6 class="mb-0">Entrega Rea/Avaluador:  
                                        @isset($avaluo->fecha_entrega_avaluador)
                                        {{  Carbon\Carbon::parse($avaluo->fecha_entrega_avaluador)->format("d-m-Y H:i:s A")  }}
                                        @endisset

                                    </h6>
                                </div>
                                <div class="mt-3">
                                    <h6 class="mb-0">Asigna/Revisi&oacute;n:  

                                        @isset($avaluo->fecha_asignacion_revisor)
                                        {{  Carbon\Carbon::parse($avaluo->fecha_asignacion_revisor)->format("d-m-Y H:i:s A")  }}
                                        @endisset
                                    </h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Revisor: @isset($avaluo->revisor->name)
                                        {{ $avaluo->revisor->name }}
                                    @endisset  @isset($avaluo->revisor->last_name)
                                    {{ $avaluo->revisor->last_name }}
                                    @endisset</h6>
                                </div>
                                

                                <div class="mt-1">
                                    <h6 class="mb-0">Entrega/Revisi&oacute;n: 
                                        @isset($avaluo->fecha_hora_revision)
                                            {{  Carbon\Carbon::parse($avaluo->fecha_hora_revision)->format("d-m-Y H:i:s A")  }}
                                        @endisset
                                    </h6>
                                </div>


                                <div class="mt-3">
                                    <h6 class="mb-0">Limite Env&iacute;o: 
                                        @isset($avaluo->fecha_entrega_cliente)
                                            {{  Carbon\Carbon::parse($avaluo->fecha_entrega_cliente)->format("d-m-Y H:i:s A")  }}
                                        @endisset
                                    </h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Fecha de Env&iacute;o: 
                                        @isset($avaluo->fecha_real_envio)
                                        {{  Carbon\Carbon::parse($avaluo->fecha_real_envio)->format("d-m-Y H:i:s A")  }}
                                        @endisset
                                        
                                    
                                    </h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">A quien se envio: {{ $avaluo->aquien }}</h6>
                                </div>
                               
                               
                                

                            </div>
                            <div class="col-lg-3">
                                <p class="font-weight-bold black">DATOS CLIENTE</p>
                                <div class="mt-1">
                                    <h6 class="mb-0">Cliente: {{ $avaluo->cliente->first_name }} {{ $avaluo->cliente->last_name }}</h6>
                                </div>
                                <div class="mt-1">
                                    <h6 class="mb-0"> @isset($avaluo->cliente->nit)
                                        Nit {{ $avaluo->cliente->nit }}
                                     @endisset
     
                                     @isset($avaluo->cliente->identificacion)
                                     Identificaci&oacute;n: {{ $avaluo->cliente->identificacion }}
                                     @endisset</h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Soporte ID:
                                        @if ($avaluo->avaluo_solicitud)
                                            @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                                <p><a target="__blank" href="{{ asset($adj->file_identificacion) }}">{{$adj->file_identificacion}}</a></p>
        
                                            @endforeach
                                        @else
                                            No hay archivos
                                        @endif
                                    </h6>
                                </div>

                                <div class="mt-1">
                                    <h6 class="mb-0">Teléfono: {{ $avaluo->cliente->telefono }}</h6>
                                </div>
            
                                <div class="mt-1">
                                    <h6 class="mb-0">Celular: {{ $avaluo->cliente->celular }}</h6>
                                </div>
            
                                <div class="mt-1">
                                    <h6 class="mb-0">Correo: {{ $avaluo->cliente->email }}</h6>
                                </div>

                                <p class="font-weight-bold mt-2 black">DATOS CONTACTO</p>

                                <div class="mt-1">
                                    <h6 class="mb-0">Contacto: {{ $avaluo->avaluo_solicitud->contacto }}</h6>
                                </div>
                
                                <div class="mt-1">
                                    <h6 class="mb-0">Correo: {{ $avaluo->avaluo_solicitud->correo }}</h6>
                                </div>
                
                                <div class="mt-1">
                                    <h6 class="mb-0">Nombre: {{ $avaluo->avaluo_solicitud->nombre }}</h6>
                                </div>
                
                                <p class="mt-5 font-weight-bold black">SOPORTE AVALUOS</p>
                                <div class="mt-1">
                                    @if ($avaluo->file_avaluo)
                                        @foreach ($avaluo->file_avaluo as $adj)
                                    <p><a target="__blank" href="{{ asset($adj->informe) }}">{{$adj->informe}}</a></p>
                                        @endforeach
                                    @else
                                        No hay archivos
                                    @endif
                                </div>
                                <div class="mt-1">
                                    <h6 class="mb-0">Archivo de avaluo enviado:  <a href="{{ asset($avaluo->informe) }}" target="__blank">
                                            <i class="fa fa-download"></i>
                                        </a></h6>
                                </div>
                                <p class="mt-5 font-weight-bold black">DATOS FACTURACI&Oacute;N</p>
                                <div class="mt-1">
                                    <h6 class="mb-0">Valor comercial: {{ $avaluo->valor_comercial }}</h6>
                                </div>
                                <div class="mt-1">
                                    <h6 class="mb-0">Facturado: @isset($avaluo->fecha_facturacion)
                                        {{  Carbon\Carbon::parse($avaluo->fecha_facturacion)->format("d-m-Y H:i:s A")  }}
                                    @endisset 
                                @empty(
                                    $avaluo->fecha_facturacion
                                )
                                    Sin fecha facturaci&oacute;n
                                @endempty
                                </h6>
                                </div>

                            </div>

                            <div class="col-lg-5">
                                <div class="card">
                    
                                    <div class="card-content collapse show">
                                        <div class="card-body overflow-scroll" style="height: 580px">
                                            <h4 class="card-title">Observaciones del caso</h4>
                                            <div class="list-group">
                    
                                                @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                                                    <a href="#" class="list-group-item list-group-item-action">
                                                        <div class="d-flex w-100 justify-content-between">
                                                            <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                                            <small class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                                        </div>
                                                        <p class="mb-1">{{ $observacion->observacion }}</p>
                                                        {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}
                    
                                                    </a>
                                                @empty
                    
                                                @endforelse
                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

      
       
    </div>
@endsection
