@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
@endpush

@section('content')
    <div class="content-body">
        <!-- Data list view starts -->
        <section id="data-list-view" class="data-list-view-header">
            <div class="row">
                <div class="col-md-4 col-12 mb-1">
                    <h5 class="text-bold-500">Fecha inicio</h5>
                    <form>
                        <input type='text' class="form-control pickadate-translations_ini" id="txtMin"/>
                    </form>
                </div>

                <div class="col-md-4 col-12 mb-1">
                    <h5 class="text-bold-500">Fecha final</h5>
                    <form>
                        <input type='text' class="form-control pickadate-translations_fin" id="txtMax" />
                    </form>
                </div>
                <div class="col-md-4 col-12 mb-1">
                    <h5 class="text-bold-500">Buscardor de fechas</h5>
                    <button id="btnGo" type="button" class="btn  btn-outline-primary">filtrar por fecha</button>
                </div>

            </div>
            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table zero-configuration">
                    <thead>
                    <tr>
                        <th>CASO No</th>
                        <th>CLIENTE</th>
                        <th>CREADO</th>
                        <th>FECHA VISITA</th>
                        <th>FECHA ENTREGA</th>
                        <th>FECHA ENTREGA REAL</th>
                        <th>FECHA A/REVISION</th>
                        <th>FECHA E/REVISION</th>
                        <th>FECHA ENVIO</th>
                        <th>FECHA REAL/ENVIO</th>

                        <th>ESTATUS</th>
                        <th>IDENTIFICACION</th>
                        <th>ACCIÓN</th>
                    </tr>
                    </thead>

                    <tfoot>
                    <tr>
                        <th>CASO No</th>
                        <th>CLIENTE</th>
                        <th>CREADO</th>
                        <th>FECHA VISITA</th>
                        <th>FECHA ENTREGA</th>
                        <th>FECHA ENTREGA REAL</th>
                        <th>FECHA ASIG./REVISION</th>
                        <th>FECHA ENT./REVISION</th>
                        <th>FECHA ENVIO</th>
                        <th>FECHA REAL/ENVIO</th>

                        <th>ESTATUS</th>
                        <th>IDENTIFICACION</th>
                        <th>ACCIÓN</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach ($avaluos as $key => $row)
                        <tr>
                            <td class="product-name">{{ $row->numero_avaluo }}</td>


                            <td>
                                <div class="chip chip-warning">
                                    <div class="chip-body">
                                        <div class="chip-text">{{ $row->cliente->first_name }}  {{ $row->cliente->last_name }}</div>
                                    </div>
                                </div>
                            </td>
                            <td class="product-category">
                                @isset($row->created_at)
                                {{  Carbon\Carbon::parse($row->created_at)->format("d-m-Y")  }}
                                @endisset
                            </td>
                            <td class="product-category">
                                @isset($row->fecha_visita)
                                {{  Carbon\Carbon::parse($row->fecha_visita)->format("d-m-Y")  }} - {{ $row->hora_visita }}
                                @endisset
                            </td>
                            <td class="product-category">
                                @if ($row->fecha_entrega_informe)
                                {{ Carbon\Carbon::parse($row->fecha_entrega_informe)->format("d-m-Y")  }}
                                @endif
                            </td>
                            <td class="product-category">
                                @isset($row->fecha_entrega_avaluador)
                                {{  Carbon\Carbon::parse($row->fecha_entrega_avaluador)->format("d-m-Y")  }}
                                @endisset
                            </td>
                            <td class="product-category">
                                @isset($row->fecha_asignacion_revisor)
                                {{  Carbon\Carbon::parse($row->fecha_asignacion_revisor)->format("d-m-Y")  }}
                                @endisset
                            </td>
                            <td class="product-category">
                                @isset($row->fecha_hora_revision)
                                {{  Carbon\Carbon::parse($row->fecha_hora_revision)->format("d-m-Y")  }}
                                @endisset
                            </td>
                            <td class="product-category">
                                @isset($row->fecha_envio)
                                {{  Carbon\Carbon::parse($row->fecha_envio)->format("d-m-Y")  }}
                                @endisset
                            </td>
                            <td class="product-category">
                                @isset($row->fecha_real_envio)
                                {{  Carbon\Carbon::parse($row->fecha_real_envio)->format("d-m-Y")  }}
                                @endisset
                            </td>

                                               <td>
                                                 @if($row->status == 0)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#">En proceso</a>
                                                @elseif($row->status == 1)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador asignado para visita</a>
                                                @elseif($row->status == 2)
                                                        <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Visita realizada con exito</a>
                                                @elseif($row->status == 3)
                                                        <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador no realizo la visita</a>
                                                @elseif($row->status == 4)
                                                        <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador reasignado para visita</a>
                                                @elseif($row->status == 5)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Proceso exitoso para facturar</a>
                                                @elseif($row->status == 6)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Devuelta por revision</a>
                                                @elseif($row->status == 7)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Correcciones para revisión</a>
                                                @elseif($row->status == 8)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluo enviado al cliente</a>
                                                @elseif($row->status == 9)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> PQRS-ENTIDAD</a>
                                                @elseif($row->status == 10)
                                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Por revision</a>
                                                @endif
                                               </td>
                            <td>
                                {{ $row->cliente->identificacion }}
                            </td>
                            <td>

                                @if (Auth::user()->hasRole(['Avaluador']))
                                    <a class="btn btn-primary" href="{{ route('getAvaluoDetalles', ['id' => $row->id]) }}">
                                        <i class="fa fa-street-view"></i>
                                    </a>
                                @else
                                @endif


                             @if (Auth::user()->hasAnyPermission(['cliente.editar']))
                                <a class="btn btn-primary" href="{{ route('datos-cliente.edit', ['id' => $row->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @else
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- DataTable ends -->
        </section>
        <!-- Data list view end -->

    </div>
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

    <!-- END: Page Vendor JS-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>

    <script>

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {


                var valid = true;
                var min = moment($("#txtMin").val());
                if (!min.isValid()) { min = null; }

                var max = moment($("#txtMax").val());
                if (!max.isValid()) { max = null; }

                if (min === null && max === null) {
                    // no filter applied or no date columns
                    valid = true;
                }
                else {

                    $.each(settings.aoColumns, function (i, col) {

                        if (col.type == "date") {
                            var cDate = moment(data[i]);

                            if (cDate.isValid()) {
                                if (max !== null && max.isBefore(cDate)) {
                                    valid = false;
                                }
                                if (min !== null && cDate.isBefore(min)) {
                                    valid = false;
                                }
                            }
                            else {
                                valid = false;
                            }
                        }
                    });
                }
                return valid;
            });

        $(document).ready(function() {
            var table =  $('.zero-configuration').DataTable({
                columns:[

                    {name:"CASO No"},
                    {name:"FECHA CREACIÓN", type:"date"},
                    {name:"CLIENTE"},
                     {name:"ESTATUS"},
                    {name:"IDENTIFICACION"},
                    {name:"ACCIÓN"}
                    ]}
            );

            // Add event listeners to the two range filtering inputs
            $("#btnGo").click(function () {
                $('.zero-configuration').DataTable().draw();
            });
        } );
        $('.pickadate-translations_ini, .pickadate-translations_fin').datepicker({
            format: 'yyyy/mm/dd'
        });

        const swalWithBootstrapButtons = swal.mixin({
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-primary',
            buttonsStyling: false,
        })
        function destroyEntidad (id) {
            swalWithBootstrapButtons({
                title: 'Desea eliminar el registro?',
                text: "Al eliminar esto no hay vuelta atras!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, eliminar!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                //Acepto eliminar el registro
                if (result.value) {
                    fetch('/entidades/' + id + '/destroy', {
                        method: 'delete',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).then(response =>
                        response.json().then(json => {
                            sweetAlert('Eliminado', json.message, 'success');
                            setTimeout(() => {
                                window.location.reload(true)
                            }, 2000);
                        })
                    );
                } else if (
                    result.dismiss === swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons(
                        'Cancelada',
                        'La operacion a sido cancelada',

                        'error'
                    )
                }
            })
        }
    </script>
@endpush
