<form method="POST" action="{{ route('casos.store') }}" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="numero_avaluo" value="{{ $generate_id }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Nombre</label>
                <div class="controls">
                    <input type="text" name="name" class="form-control"
                           data-validation-required-message="Este campo es requerido" placeholder="Nombre"
                           value="{{  old('name') }}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
            </div>

            <div class="form-group apellido">
                <label>Apellido</label>
                <div class="controls">
                    <input type="text" name="last_name" class="form-control"
                         placeholder="Apellido"
                        value="{{  old('last_name') }}">
                </div>
            </div>
            <div class="form-group">
                <label>Telefono</label>
                <div class="controls">
                    <input type="text" name="telefono" class="form-control" placeholder="Telefono"
                           value="{{  old('telefono') }}">
                </div>
            </div>

            <div class="form-group">
                <label>Celular</label>
                <div class="controls">
                    <input type="text" name="celular" class="form-control" required
                           data-validation-required-message="Este campo es requerido" placeholder="Celular"
                           value="{{  old('celular') }}">
                    @error('celular')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label>Corre electronico</label>
                <div class="controls">
                    <input type="text" name="email" class="form-control" placeholder="Corre electronico"
                           value="{{  old('email') }}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <label>
                        Departamento
                    </label>
                    <div class="form-group">
                        {{ Form::select('departament_id', $departamentos->pluck('nombre', 'id'), null, ['placeholder' => 'Por favor seleccione', 'class' => 'select2-size-lg form-control', 'id' => 'departament_id', 'required' => 'required', 'data-validation-required-message' => 'Este campo es requerido']) }}
                        @error('departament_id')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Ciudad o Municipio</label>
                <div class="controls">
                    <select class="form-control" name="city_id" id="city"></select>

                </div>
            </div>
            <div class="form-group">
                <label>Direccion</label>
                <textarea class="form-control" id="direccion" rows="3" name="direccion" placeholder="Direccion"
                          data-validation-required-message="Este campo es requerido"
                          value="{{  old('direccion') }}"></textarea>
                @error('direccion')
                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                @enderror
            </div>
            <div class="form-group">
                <label>Tipo predio</label>
                <div class="controls">
                    {{ Form::select('type_predio_id', $tipo_predios->pluck('nombre', 'id'), null, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'type_predio_id']) }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Tipo cliente</label>
                <div class="controls">
                    {{ Form::select('tipo_cliente_id', $tipo_clientes->pluck('name', 'id'), null, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'tipo_cliente_id']) }}
                    @error('tipo_cliente_id')
                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror
                </div>
            </div>
            <div class="form-group identificacion">
                <label>Identificacion</label>
                <div class="controls">
                    <input type="text" name="identificacion" class="form-control" placeholder="Identificacion"
                           value="{{  old('identificacion') }}">
                </div>
            </div>

            <div class="form-group nit">
                <label>Nit</label>
                <div class="controls">
                    <input type="text" name="nit" class="form-control" placeholder="Nit" value="{{  old('nit') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <div class="custom-file">
                        <input type="file" name="file_identificacion[]" class="custom-file-input" id="customFileLang"
                               lang="es" multiple>
                        <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                            Identificación</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Certificación Tradición</label>
                <div class="controls">
                    <input type="text" name="certificacion" class="form-control" required
                           data-validation-required-message="Este campo es requerido"
                           placeholder="Certificación Tradición" value="{{  old('certificacion') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <div class="custom-file">
                        <input type="file" name="file_certificacion[]" class="custom-file-input" id="customFileLang"
                               lang="es" multiple>
                        <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                            Certificación</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Escritura publica (Ultima adquisición)</label>
                <div class="controls">
                    <input type="text" name="escritura" class="form-control" required
                           data-validation-required-message="Este campo es requerido"
                           placeholder="Escritura publica (Ultima adquisición)" value="{{  old('soporte') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="custom-file">
                    <input type="file" name="file_escritura[]" class="custom-file-input" id="customFileLang" lang="es" multiple>
                    <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de Escritura</label>
                </div>
            </div>

            <div class="form-group">
                <label>Datos facturacion son los mismos del cliente?</label>
                <div class="controls">
                    <input type="checkbox" name="check" id="check" value="1" onchange="javascript:showContent()"/>
                    <span class="text-danger ml-1">Seleccion si son los mismo</span>
                    <p>Nota: Al seleccionar algunas casillas de abajo desapareceran (Datos de facturación)</p>
                </div>
            </div>

            <div class="form-group">
                <label>Datos facturacion ITAÚ?</label>
                <div class="controls">
                    <input type="checkbox" name="check_itaud" id="check_itaud" value="1"
                           onchange="javascript:showContentItaud()"/>
                    <span class="text-danger ml-1">Seleccion si eres de banco ITAÚ</span>
                    <p>Nota: Al seleccionar algunas casillas de abajo desapareceran y otros se autorrelenaran</p>
                </div>
            </div>
        </div>
    </div>

    <div class="alert alert-primary mb-2" role="alert">
        <strong>DATOS</strong> FACTURACIÓN.
    </div>

    <div class="row">
        <div class="col-md-6">

            <div id="content">
                <div class="form-group">
                    <label>Nombre factura</label>
                    <div class="controls">
                        <input type="text" id="factura" name="factura" class="form-control" placeholder="Nombre factura"
                               value="{{  old('factura') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Telefono</label>
                    <div class="controls">
                        <input type="text" id="telefono_factura" name="telefono_factura" class="form-control"
                               placeholder="Telefono" value="{{  old('telefono_factura') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Correo</label>
                    <div class="controls">
                        <input type="email" id="email_factura" name="email_factura" class="form-control"
                               placeholder="Correo" value="{{  old('email_factura') }}">
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="direccion_factura" rows="2" name="direccion_factura"
                              placeholder="Direccion" value="{{  old('direccion_factura') }}"></textarea>
                </div>
                <div class="form-group">
                    <label>Identificacion</label>
                    <div class="controls">
                        <input type="text" id="identificacion_factura" name="identificacion_factura"
                               class="form-control" placeholder="Identificacion"
                               value="{{  old('identificacion_factura') }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" name="file_identificacion_factura" class="custom-file-input"
                               id="customFileLang" lang="es">
                        <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                            Identificacion</label>
                    </div>
                </div>
            </div>
            <div id="itaud_content">
                <div class="form-group">
                    <label>Banco (Consigno)</label>
                    <div class="controls">
                        <input type="text" name="banco_consigno" class="form-control" placeholder="Banco (Consigno)"
                               value="{{  old('banco_consigno') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label>Valor anticipo</label>
                    <div class="controls">
                        <input type="text" name="valor_anticipo" class="form-control" placeholder="Valor anticipo"
                               onkeyup="format(this)" onchange="format(this)" value="{{  old('valor_anticipo') }}">
                        @error('valor_anticipo')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label>Valor anticipo</label>
                    <div class="controls">
                        <div class="custom-file">
                            <input type="file" name="file_anticipo" class="custom-file-input" id="customFileLang"
                                   lang="es">
                            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                                Anticipo</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>

