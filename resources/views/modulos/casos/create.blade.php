@extends('layouts.app')


@section('content')

    <div class="content-body">
        <!-- Input Validation start -->
        <section class="input-validation">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Gestionar datos del cliente</h4>
                            Numero avaluo: <span class="text-danger text-left">AV{{ $generate_id }}</span>
                            Fecha: <span class="text-danger text-right">{{ date('Y-m-d H:m:s a') }}</span>
                        </div>
                        <div class="card-content">
                            <div class="card-body">

                                @include('modulos.casos._form')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Input Validation end -->
    </div>

@endsection

@push('scripts')
    <script>
        $("#departament_id").change(function (e) {
            let departament_id = e.target.value
            $("#city").empty();//To reset cities
            $("#city").append("<option>Por favor seleccione</option>");
            $.get('/cities/' + departament_id + '/show', function (response) {
                $(response).each(function (i) {//to list cities
                    $("#city").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>")
                });
            });
        });

        function format(input) {
            var num = input.value.replace(/\./g, '');
            if (!isNaN(num)) {
                num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
                num = num.split('').reverse().join('').replace(/^[\.]/, '');
                input.value = num;
            } else {
                alert('Solo se permiten numeros');
                input.value = input.value.replace(/[^\d\.]*/g, '');
            }
        }

        $("#tipo_cliente_id").change(function (e) {
            let tipo_cliente_id = e.target.value
            let nit = document.querySelector('.nit');
            let identificacion = document.querySelector('.identificacion');
            let apellido = document.querySelector('.apellido');
            if (tipo_cliente_id == 1) {
                nit.style.display = "none";
                identificacion.style.display = "block";
                apellido.style.display = "block";
            } else {
                identificacion.style.display = "none";
                nit.style.display = "block";
                apellido.style.display = "none";
            }
        });

        function showContentItaud() {
            element = document.getElementById("itaud_content");
            check = document.getElementById("check_itaud");
            if (check.checked) {
                document.getElementById("factura").value = "Itaú Corbanca Colombia S.A";
                document.getElementById("telefono_factura").value = "5818181";
                document.getElementById("email_factura").value = "asesorhipotecario1@cibergestion.com.co";
                document.getElementById("direccion_factura").value = "Calle 100 No. 7-25";
                document.getElementById("identificacion_factura").value = "890903937-0";
                element.style.display = 'none';
            } else {
                document.getElementById("factura").value = "";
                document.getElementById("telefono_factura").value = "";
                document.getElementById("email_factura").value = "";
                document.getElementById("direccion_factura").value = "";
                document.getElementById("identificacion_factura").value = "";
                element.style.display = 'block';
            }

        }

        function showContent() {
            element = document.getElementById("content");
            check = document.getElementById("check");
            if (check.checked) {
                element.style.display = 'none';
            } else {
                element.style.display = 'block';
            }
        }
    </script>
@endpush
