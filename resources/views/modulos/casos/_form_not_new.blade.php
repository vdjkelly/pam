<form method="POST" action="{{ route('casos.getInfoSolicitante') }}" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @isset($generate_id)
        <input type="hidden" name="numero_avaluo" value="AV{{ $generate_id }}">
    @endisset

    <input type="hidden" name="user_id" value="{{ $cliente->id }}">
    <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="controls">
                    <label>
                        Entidad
                    </label>
                    <div class="form-group">
                        {{ Form::select('entidad_id', $entidades->pluck('nombre', 'id'), $avaluo->entidad_id, ['placeholder' => 'Por favor seleccione', 'class' => 'select2-size-lg form-control', 'id' => 'entidad_id', 'required' => 'required', 'data-validation-required-message' => 'Este campo es requerido']) }}
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="controls">
                    <label>
                        Tipo de avaluo
                    </label>
                    <div class="form-group">
                        {{ Form::select('tipo_avaluo_id', $tipo_avaluos->pluck('nombre', 'id'), $avaluo->tipo_avaluo_id, ['placeholder' => 'Por favor seleccione', 'class' => 'select2-size-lg form-control', 'id' => 'tipo_avaluo_id', 'required' => 'required', 'data-validation-required-message' => 'Este campo es requerido']) }}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Consecutivo entidades</label>
                <div class="controls">
                    <input type="text" name="consecutivo_entidades" class="form-control"
                           placeholder="Consecutivo entidades"
                           value="{{ $avaluo->avaluo_solicitud->consecutivo_entidades }}">
                </div>
            </div>


            <div class="form-group">
                <label>Contacto</label>
                <div class="controls">
                    <input type="text" name="contacto" class="form-control"
                           placeholder="Contacto"
                           value="{{ $avaluo->avaluo_solicitud->contacto }}">
                </div>
            </div>

            <div class="form-group">
                <label>Correo contacto</label>
                <div class="controls">
                    <input type="email" name="correo" class="form-control" placeholder="Correo contacto"
                           value="{{ $avaluo->avaluo_solicitud->correo }}">
                </div>
            </div>

            <div class="form-group">
                <label>Teléfono Contacto</label>
                <div class="controls">
                    <input type="text" name="telefono_contacto" class="form-control"
                           data-validation-required-message="Este campo es requerido" placeholder="Teléfono contacto"
                           value="{{ $avaluo->avaluo_solicitud->telefono_contacto }}">
                </div>
            </div>



        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Nombre</label>
                <div class="controls">
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre"
                           value="{{ $cliente->first_name }}">
                </div>
            </div>
            @isset($cliente->identificacion)
                <div class="form-group">
                    <label>Identificacion</label>
                    <div class="controls">
                        <input type="text" class="form-control" name="identificacion" placeholder="Identificacion"
                               value="{{ $cliente->identificacion }}">
                    </div>
                </div>
            @endisset


            @isset($cliente->nit)
                <div class="form-group">
                    <label>NIT</label>
                    <div class="controls">
                        <input type="text" class="form-control" name="nit" placeholder="NIT"
                               value="{{ $cliente->nit }}">
                    </div>
                </div>
            @endisset


            <div class="form-group">
                <label>Celular</label>
                <div class="controls">
                    <input type="text" class="form-control" name="celular" placeholder="Celular"
                           value="{{ $cliente->celular }}">
                </div>
            </div>

             <div class="form-group">
                <label>Correo</label>
                <div class="controls">
                    <input type="email" name="email" class="form-control"
                           placeholder="Correo cliente"
                           value="{{ $cliente->email }}">
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-primary mb-2" role="alert">
        <strong>ASIGNACIÓN</strong>
    </div>
    <div class="row">
        <div class="col-md-6">


            <div class="controls">
                <label>
                    Fecha visita
                </label>
                <div class="form-group">
                    <input type='text' name="fecha_visita" class="form-control format-picker-fecha"/>
                </div>
            </div>

            <div class="card overflow-auto" style="height: 517.683px;">
                <div class="card-header">
                    <h4 class="card-title">Observaciones del caso</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="list-group">

                            @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                                <a href="#" class="list-group-item list-group-item-action">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                        <small
                                            class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                    </div>
                                    <p class="mb-1">{{ $observacion->observacion }}</p>
                                    {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}

                                </a>
                            @empty

                            @endforelse

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6">


            <div class="form-group">
                <label>Avaluador</label>
                <div class="controls">
                    {{ Form::select('avaluador_id', $avaluadores->pluck('name', 'id'), null, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'type_predio_id']) }}
                </div>
            </div>
            <div class="form-group">
                <label>Hora visita</label>
                <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                    <input type="text" name="hora_visita" data-target="#datetimepicker3" data-toggle="datetimepicker"
                           class="form-control datetimepicker-input" data-target="#datetimepicker3"/>
                    <div class="input-group-append">
                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                    </div>
                </div>
            </div>

            <div class="form-group">

                <textarea class="form-control" id="observacion" rows="2" name="observacion" placeholder="Observación"
                          value="{{  old('observacion') }}"></textarea>
            </div>

            <div class="controls">
                <label>
                    Fecha entrega informe
                </label>
                <div class="form-group">
                    <input type='text' name="fecha_entrega_informe"
                           class="form-control format-picker-fecha_entrega_informe"/>
                </div>
            </div>


        </div>
    </div>

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
