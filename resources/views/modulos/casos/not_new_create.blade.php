@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css">
    <style>
        html body .content.app-content {
            overflow: auto;
        }
    </style>
@endpush

@section('content')

    <div class="content-body">
        <!-- Input Validation start -->
        <section class="input-validation">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Informacion solicitante</h4>
                            Numero avaluo: <span class="text-danger text-left">{{ $avaluo->numero_avaluo }}</span>
                            Fecha: <span class="text-danger text-right">{{ date('Y-m-d H:m:s a') }}</span>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                @include('modulos.casos._form_not_new')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Input Validation end -->
    </div>

@endsection

@push('scripts')
    <script>
        $("#departament_id").change(function (e) {
            let departament_id = e.target.value
            $("#city").empty();//To reset cities
            $("#city").append("<option>Por favor seleccione</option>");
            $.get('/cities/' + departament_id + '/show', function (response) {
                $(response).each(function (i) {//to list cities
                    $("#city").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>")
                });
            });
        });
        /* (function (window, document, $) {
             $.get('/cities/'+ {{ $avaluo->departament_id }} + '/show', function(response) {
                $(response).each(function(i){//to list cities
                    $("#city").append("<option value='" + response[i].id + "'>"+ response[i].nombre +"</option>")
                });
            });
        })(window, document, jQuery);*/
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

    <script>
        var getDate = function (input) {
            return new Date(input.date.valueOf());
        }
        $('.format-picker-fecha, .format-picker-fecha_entrega_informe').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('.format-picker-fecha_entrega_informe').datepicker({
            startDate: '+6d',
            endDate: '+36d',
        });

        $('.format-picker-fecha').datepicker({
            startDate: '+5d',
            endDate: '+35d',
        }).on('changeDate',
            function (selected) {
                $('.format-picker-fecha_entrega_informe').datepicker('clearDates');
                $('.format-picker-fecha_entrega_informe').datepicker('setStartDate', getDate(selected));
            });
    </script>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css"/>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker3').datetimepicker({
                format: 'LT'
            });
        });

        $("#departament_id").change(function (e) {
            let departament_id = e.target.value
            $("#city").empty();//To reset cities
            $("#city").append("<option>Por favor seleccione</option>");
            $.get('/cities/' + departament_id + '/show', function (response) {
                $(response).each(function (i) {//to list cities
                    $("#city").append("<option value='" + response[i].id + "'>" + response[i].nombre + "</option>")
                });
            });
        });
    </script>
@endpush
