@extends('layouts.app')

@push('styles')

@endpush

@section('content')
<div class="row">
    <div class="col-md-6 col-sm-12">
            <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">INFORMACIÓN AVALUO </h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                                <form method="POST" action="{{ route('revisor.getStoreRevisor') }}" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">

                                    <div class="mt-1">
                                        <h6 class="mb-0">Numero avaluo:  {{ $avaluo->numero_avaluo }}</h6>
                                    </div>

                                    <div class="mt-1">
                                        <h6 class="mb-0">
                                            Consecutivo Entidades: {{ $avaluo->avaluo_solicitud->consecutivo_entidades }} </h6>
                                    </div>
            
                                    <div class="mt-1">
                                        <h6 class="mb-0">
                                            Tipo Avaluo: {{ $avaluo->tipo_avaluo->nombre }} </h6>
                                    </div>

                                    <div class="mt-1">
                                        <h6 class="mb-0">Fecha limite de envío al cliente: {{ \Carbon\Carbon::parse($avaluo->fecha_entrega_cliente)->format('Y-m-d') }} </h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">NOMBRE: {{ $avaluo->cliente->first_name }} {{ $avaluo->cliente->last_name }}</h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Tipo predio: @isset($avaluo->type_predio)
                                            {{ $avaluo->type_predio->nombre }}
                                        @endisset </h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Entidad: {{ $avaluo->entidad->nombre }}</h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Soporte ID:
                                                @if ($avaluo->avaluo_solicitud)
                                                @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                                    <p><a target="__blank" href="{{ asset($adj->file_identificacion) }}">{{$adj->file_identificacion}}</a></p>

                                                @endforeach
                                            @else
                                                No hay archivos
                                            @endif

                                        </h6>
                                    </div>

                                    <div class="mt-1">
                                        <h6 class="mb-0">Soporte CTL:
                                                @if ($avaluo->avaluo_solicitud)
                                                @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                                    <p><a target="__blank" href="{{ asset($adj->file_certificacion) }}">{{$adj->file_certificacion}}</a></p>

                                                @endforeach
                                            @else
                                                No hay archivos
                                            @endif
                                        </h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Soporte escritura:
                                                @if ($avaluo->avaluo_solicitud)
                                                @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                                    <p><a target="__blank" href="{{ asset($adj->file_escritura) }}">{{$adj->file_escritura}}</a></p>
                                                @endforeach
                                            @else
                                                No hay archivos
                                            @endif
                                        </h6>
                                    </div>

                                    <div class="mt-1">
                                        <h6 class="mb-0">Avaluador: @isset($avaluo->avaluador)
                                            {{ $avaluo->avaluador->name }} {{ $avaluo->avaluador->last_name }}
                                        @endisset </h6>
                                    </div>
                                    <h4 class="card-title mt-5">Gestion revisor técnico</h4>
                                    <div class="form-group">
                                        <label>Valor comercial</label>
                                        <div class="controls">
                                            <input type="text" name="valor_comercial" class="form-control" required data-validation-required-message="Este campo es requerido" placeholder="90.000" value="{{ $avaluo->valor_comercial }}" onkeyup="format(this)" onchange="format(this)">
                                        </div>
                                    </div>


                                    <div class="mt-1 mb-2">
                                        <h6 class="mb-0">Descarga archivos adjuntos:
                                            @if ($avaluo->file_avaluo)
                                                @foreach ($avaluo->file_avaluo as $adj)
                                            <p><a target="__blank" href="{{ asset($adj->informe) }}">{{$adj->informe}}</a></p>
                                                @endforeach
                                            @else
                                                No hay archivos
                                            @endif
                                        </h6>
                                    </div>


                                    <div class="form-group">
                                        <div class="controls">
                                                <div class="custom-file">
                                                    <input type="file" name="informe[]" class="custom-file-input" id="customFileLang" lang="es" multiple>
                                                    <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de Informe</label>
                                                </div>
                                        </div>
                                    </div>


                                    <p>Avaluo ok?</p>
                                <ul class="list-unstyled mb-0">
                                        <li class="d-inline-block mr-2">
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" name="avaluo" id="customRadio1" checked value="1">
                                                        <label class="custom-control-label" for="customRadio1">Si</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2">
                                                <fieldset>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" name="avaluo" id="customRadio2" value="0">
                                                        <label class="custom-control-label" for="customRadio2">No</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                    </ul>





                                            <div class="form-group mb-3 mt-3">
                                                    <textarea class="form-control" id="observacion" rows="2" name="observacion" placeholder="Observaciones avaluo"></textarea>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                        </div>
                    </div>
                </div>
        </div>

    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">INFORMACIÓN AVALUO </h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <h4 class="card-title mt-5">Observaciones del caso</h4>
                    <div class="list-group">

                        @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                            <a href="#" class="list-group-item list-group-item-action">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                    <small class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                </div>
                                <p class="mb-1">{{ $observacion->observacion }}</p>
                                {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}

                            </a>
                        @empty

                        @endforelse

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script !src="">
        function format(input)
        {
            var num = input.value.replace(/\./g,'');
            if(!isNaN(num)){
                num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
                num = num.split('').reverse().join('').replace(/^[\.]/,'');
                input.value = num;
            }

            else{ alert('Solo se permiten numeros');
                input.value = input.value.replace(/[^\d\.]*/g,'');
            }
        }
    </script>
@endpush
