@extends('layouts.app')

@push('styles')

@endpush

@section('content')
<div class="row">
    <div class="col-md-6 col-sm-12">
            <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ASIGNAR A REVISIÓN </h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                                <form method="POST" action="{{ route('revisor.store') }}" class="form-horizontal" enctype="multipart/form-data">
                                    @csrf
                                <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">
                                    <div class="mt-1">
                                        <h6 class="mb-0">Fecha entrega cliente: {{ $avaluo->fecha_entrega_cliente }}</h6>
                                    </div>

                                    <div class="mt-1">
                                        <h6 class="mb-0">NOMBRE: {{ $avaluo->cliente->first_name }} {{ $avaluo->cliente->last_name }}</h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Tipo predio: @isset($avaluo->type_predio)
                                            {{ $avaluo->type_predio->nombre }}
                                        @endisset </h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Avaluador: {{ $avaluo->avaluador->name }}  {{ $avaluo->avaluador->last_name }}</h6>
                                    </div>

                                    <div class="mt-1">
                                        <h6 class="mb-0">Valor comercial: {{ $avaluo->valor_comercial }}</h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Departamento: {{ $avaluo->departament->nombre }}</h6>
                                    </div>
                                    <div class="mt-1">
                                        <h6 class="mb-0">Ciudad: {{ $avaluo->city->nombre }}</h6>
                                    </div>

                                            <div class="form-group mb-3 mt-3">
                                                    <textarea class="form-control" id="observacion" rows="2" name="observacion" placeholder="Observaciones avaluo"></textarea>
                                            </div>

                                            <div class="form-group">
                                                    <label>Revisor Técnico</label>
                                                    <div class="controls">
                                                            {{ Form::select('revisor_tecnico_id', $revisores_tecnico->pluck('name', 'id'), null, ['placeholder' => 'Por favor seleccione', 'class' => 'form-control', 'id' => 'revisor_tecnico_id']) }}
                                                    </div>
                                                </div>

                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                </form>
                        </div>
                    </div>
                </div>
        </div>

        <div class="col-md-6 col-sm-12">
                <div class="card overflow-auto" style="height: 517.683px;">
                    <div class="card-header">
                        <h4 class="card-title">Observaciones del caso</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body overflow-scroll">
                                <div class="list-group">

                                        @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                                <small class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                                </div>
                                                <p class="mb-1">{{ $observacion->observacion }}</p>
                                                {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}

                                            </a>
                                        @empty

                                        @endforelse

                                    </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@endsection

@push('scripts')

@endpush
