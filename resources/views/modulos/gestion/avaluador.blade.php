@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/data-list-view.css') }}">
@endpush

@section('content')
    <div class="content-body">
        <!-- Data list view starts -->
        <section id="data-list-view" class="data-list-view-header">

            <!-- DataTable starts -->
            <div class="table-responsive">
                <table class="table data-list-view">
                    <thead>
                        <tr>
                            <th>CASO No</th>
                            <th>FECHA CREACIÓN</th>
                            <th>CLIENTE</th>
                            <th>FECHA ENTREGA INFORME</th>
                            <th>STATUS</th>
                            <th>Gestionar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($avaluos as $key => $row)



                        <tr>
                        <td class="product-name">{{ $row->numero_avaluo }}</td>
                            <td class="product-category">{{ Carbon\Carbon::parse($row->created_at)->format('Y-m-d') }}</td>

                            <td>
                                <div class="chip chip-warning">
                                    <div class="chip-body">
                                        <div class="chip-text">{{ $row->first_name }}  {{ $row->last_name }}</div>
                                    </div>
                                </div>
                            </td>
                            <td class="product-category">{{ Carbon\Carbon::parse($row->fecha_entrega_informe)->format('Y-m-d') }}</td>
                            @if($row->status == 0)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#">En proceso</a></td>
                            @elseif($row->status == 1)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador asignado para visita</a></td>
                            @elseif($row->status == 2)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Visita realizada con exito</a></td>
                            @elseif($row->status == 3)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador no realizo la visita</a></td>
                            @elseif($row->status == 4)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluador reasignado para visita</a></td>
                            @elseif($row->status == 5)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Proceso exitoso para facturar</a></td>
                            @elseif($row->status == 6)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Devuelta por revision</a></td>
                            @elseif($row->status == 7)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Correcciones para revisión</a></td>
                            @elseif($row->status == 8)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Avaluo enviado al cliente</a></td>
                            @elseif($row->status == 9)
                                <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#">  PQRS-ENTIDAD </a></td>
                                @elseif($row->status == 10)
                                                    <td><a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Por revision</a></td>
                            @endif

                            <td>
                                @if ($row->status == 1)
                                    <a href="{{ route('casos.show', ['id' => $row->id, 'notification' => $row->notification_id]) }}" role="button" class="btn btn-icon btn-primary"><i class="feather icon-inbox"></i></a>
                                @elseif($row->status == 5)
                                    <a role="button" class="btn btn-outline-dark waves-effect waves-light text-right" href="#"> Proceso exitoso para facturar</a>
                                @elseif($row->status == 4)
                                    <a href="{{ route('casos.show', ['id' => $row->id, 'notification' => $row->notification_id]) }}" role="button" class="btn btn-icon btn-primary"><i class="feather icon-inbox"></i></a>
                                @elseif($row->status == 6)
                                    <a href="{{ route('casos.show', ['id' => $row->id, 'notification' => $row->notification_id]) }}" role="button" class="btn btn-icon btn-primary"><i class="feather icon-inbox"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- DataTable ends -->
        </section>
        <!-- Data list view end -->

    </div>
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/ui/data-list-view.js') }}"></script>

    <script>

        const swalWithBootstrapButtons = swal.mixin({
   				confirmButtonClass: 'btn btn-primary',
   				cancelButtonClass: 'btn btn-primary',
   				buttonsStyling: false,
   			})
        function destroyEntidad (id) {
             swalWithBootstrapButtons({
                            title: 'Desea eliminar el registro?',
                            text: "Al eliminar esto no hay vuelta atras!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Si, eliminar!',
                            cancelButtonText: 'No, cancelar!',
                            reverseButtons: true
                        }).then((result) => {
                            //Acepto eliminar el registro
                            if (result.value) {
                                fetch('/entidades/' + id + '/destroy', {
                                    method: 'delete',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                }).then(response =>
                                    response.json().then(json => {
                                        sweetAlert('Eliminado', json.message, 'success');
                                        setTimeout(() => {
                                            window.location.reload(true)
                                        }, 2000);
                                    })
                                );
                            } else if (
                                result.dismiss === swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons(
                                    'Cancelada',
                                    'La operacion a sido cancelada',

                                    'error'
                                )
                            }
                        })
         }
    </script>
@endpush
