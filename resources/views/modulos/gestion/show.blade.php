@extends('layouts.app')

@push('styles')

@endpush

@section('content')
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h4>Fecha de solicitud: {{ $avaluo->created_at->format('Y-m-d H:i:s A') }}</h4>
        </div>

        <div class="col-md-6 col-sm-6">
            <h4>Numero avaluo: {{ $avaluo->numero_avaluo }}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">INFORMACIÓN CONTACTO </h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="mt-1">
                            <h6 class="mb-0">Nombre y
                                Apellido Cliente: {{ $avaluo->cliente->first_name }} {{ $avaluo->cliente->last_name }}</h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">Entidad: {{ $avaluo->entidad->nombre }} </h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Contacto: {{ $avaluo->avaluo_solicitud->contacto }} </h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">Celular Contacto: {{ $avaluo->avaluo_solicitud->telefono_contacto }} </h6>
                        </div>
                        @isset($avaluo->cliente->identificacion)
                            <div class="mt-1">
                                <h6 class="mb-0">Identificaci&oacute;n: {{ $avaluo->cliente->identificacion }}</h6>
                            </div>
                        @endisset

                        @isset($avaluo->cliente->nit)
                            <div class="mt-1">
                                <h6 class="mb-0">Nit: {{ $avaluo->cliente->nit }} </h6>
                            </div>
                        @endisset

                        <div class="mt-1">
                            <h6 class="mb-0">Soporte escritura:
                                @if ($avaluo->avaluo_solicitud)
                                    @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                        <p><a target="__blank" href="{{ asset($adj->file_escritura) }}">{{$adj->file_escritura}}</a></p>
                                    @endforeach
                                @else
                                    No hay archivos
                                @endif
                            </h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">Soporte CTL:
                                @if ($avaluo->avaluo_solicitud)
                                    @foreach ($avaluo->avaluo_solicitud->file_avaluo_solicitud as $adj)
                                        <p><a target="__blank" href="{{ asset($adj->file_certificacion) }}">{{$adj->file_certificacion}}</a></p>

                                    @endforeach
                                @else
                                    No hay archivos
                                @endif
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">DATOS DEL INMUEBLE </h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="mt-1">
                            <h6 class="mb-0">Departamento: {{ $avaluo->departament->nombre }}</h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Direccion: {{ $avaluo->avaluo_solicitud->direccion }}</h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Fecha
                                visita: {{ Carbon\Carbon::parse($avaluo->fecha_visita)->format('Y-m-d') }} {{ $avaluo->hora_visita}} </h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Ciudad: @isset($avaluo->city)
                                    {{ $avaluo->city->nombre }}
                                @endisset</h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Tipo predio: @isset($avaluo->type_predio)
                                    {{ $avaluo->type_predio->nombre }}
                                @endisset</h6>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">
                                Avaluador: {{ $avaluo->avaluador->name }}  {{ $avaluo->avaluador->last_name }}</h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">
                                Consecutivo Entidades: {{ $avaluo->avaluo_solicitud->consecutivo_entidades }} </h6>
                        </div>

                        <div class="mt-1">
                            <h6 class="mb-0">
                                Tipo Avaluo: {{ $avaluo->tipo_avaluo->nombre }} </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h4>Fecha de entrega
                informe: {{ Carbon\Carbon::parse($avaluo->fecha_entrega_informe)->format('Y-m-d') }}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">GESTION AVALUADOR </h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <form method="POST" action="{{ route('gestion.store') }}" class="form-horizontal"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="avaluo_id" value="{{ $avaluo->id }}">
                            <input type="hidden" name="notification_id" value="{{ $notification }}">
                            @if ($avaluo->visita == false)
                                <p>SE REALIZO LA VISITA?</p>
                                <ul class="list-unstyled mb-0">
                                    <li class="d-inline-block mr-2">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input visita" name="visita"
                                                       id="customRadio1" checked value="1">
                                                <label class="custom-control-label" for="customRadio1">Si</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input visita" name="visita"
                                                       id="customRadio2" value="0">
                                                <label class="custom-control-label" for="customRadio2">No</label>
                                            </div>
                                        </fieldset>
                                    </li>
                                </ul>
                            @endif


                            <div id="content">
                                <fieldset class="form-group">
                                    <label for="basicInput">Valor comercial</label>
                                    <input type="text" class="form-control" name="valor_comercial" id="valor_comercial"
                                           placeholder="400.00" onkeyup="format(this)" onchange="format(this)">
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="custom-file">
                                        <input type="file" name="informe[]" class="custom-file-input"
                                               id="customFileLang" lang="es" multiple>
                                        <label class="custom-file-label" for="customFileLang">Seleccionar Archivo de
                                            Informe</label>
                                    </div>
                                </fieldset>


                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="observacion" rows="2" name="observacion"
                                          placeholder="Observaciones avaluo" required></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="card" style="height: 517.683px;">
                <div class="card-header">
                    <h4 class="card-title">Observaciones del caso</h4>
                </div>
                <div class="card-content overflow-scroll">
                    <div class="card-body">
                        <div class="list-group">

                            @forelse ($avaluo->avaluao_observacion as $key => $observacion)
                                <a href="#" class="list-group-item list-group-item-action">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{ $observacion->user->name }} {{ $observacion->user->last_name }}</h5>
                                        <small
                                            class="text-muted">{{ $observacion->created_at->diffForHumans() }}</small>
                                    </div>
                                    <p class="mb-1">{{ $observacion->observacion }}</p>
                                    {{-- <small class="text-muted">Donec id elit non mi porta.</small> --}}

                                </a>
                            @empty

                            @endforelse

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $(".visita").click(function (evento) {

                var valor = $(this).val();

                if (valor == 0) {
                    $("#content").css("display", "none");
                } else {
                    $("#content").css("display", "block");
                }
            });
        });

        function format(input) {
            var num = input.value.replace(/\./g, '');
            if (!isNaN(num)) {
                num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
                num = num.split('').reverse().join('').replace(/^[\.]/, '');
                input.value = num;
            } else {
                alert('Solo se permiten numeros');
                input.value = input.value.replace(/[^\d\.]*/g, '');
            }
        }
    </script>
@endpush
