@isset($role)
{!! Form::model($role, ['route' => ['roles.update', $role->id], 'class' => 'form-horizontal']) !!}
{{ method_field('PATCH') }}
@endisset

@empty($role)
    {!! Form::open(['route' => 'roles.store', 'class' => 'form-horizontal']) !!}
@endempty
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Role</label>
                <div class="controls">
                    <input type="text" name="name" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Secretaria" value="@isset($role){{ $role->name }}@endisset @empty($user){{ old('name') }}@endempty">
                    @error('name')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="divider divider-danger">
                <div class="divider-text">Lista de permisos</div>
            </div>
            <div class="form-group">
                <ul class="list-unstyled mb-0">
                    @foreach ($permissions as $key => $row)
                        <div class="col-lg-6">
                            <li class="d-inline-block mr-2">
                                <fieldset>
                                    <div class="custom-control custom-checkbox">
                                        {{ Form::checkbox('permission[]', $row->id, in_array($row->id, $rolePermissions) ? true : false, array('class' => 'custom-control-input', 'id' => 'permission_'.$row->id)) }}
                                        <label class="custom-control-label" for="permission_{{ $row->id }}">{{ $row->description }}</label>
                                    </div>
                                </fieldset>
                            </li>
                        </div>
                    @endforeach
                    
                   {{--  <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="customCheck" id="customCheck2">
                                <label class="custom-control-label" for="customCheck2">Inactive</label>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked name="customCheck" id="customCheck3" disabled>
                                <label class="custom-control-label" for="customCheck3">Active - disabled</label>
                            </div>
                        </fieldset>

                    </li>
                    <li class="d-inline-block">
                        <fieldset>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="customCheck" disabled id="customCheck4">
                                <label class="custom-control-label" for="customCheck4">Inactive - disabled</label>
                            </div>
                        </fieldset>
                    </li> --}}
                   
                </ul>

                @error('permission')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
{!! Form::close() !!}