@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
@endpush

@section('content')
     <!-- Zero configuration table -->
     <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Roles</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <p class="card-text text-right">
                                    <a role="button" href="{{ route('roles.create') }}" class="btn btn-primary"><i class="feather icon-check"></i> Agregar</a>
                                </p>
                                <div class="table-responsive">
                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($roles as $row)
                                            <tr>
                                                <td>{{ $row->name }}</td>
                                                <td>
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ route('roles.edit', ['id' => $row->id]) }}" type="button" class="btn btn-primary"><i class="feather icon-edit"></i></a>
                                                    <a onclick="destroyRole({{ $row->id }})" type="button" class="btn btn-primary"><i class="feather icon-trash white"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @empty
                                                
                                            @endforelse
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Acción</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ Zero configuration table -->
@endsection

@push('scripts')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

    <script>
         $('.zero-configuration').DataTable({
            "language": {
    "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados puedes crear un <a role='button' class='btn btn-outline-primary waves-effect waves-light text-left' href='{{ route('casos.create') }}'>Nuevo</a> cliente",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
}
        });
        const swalWithBootstrapButtons = swal.mixin({
   				confirmButtonClass: 'btn btn-primary',
   				cancelButtonClass: 'btn btn-primary',
   				buttonsStyling: false,
   			})
        function destroyRole (id) {
             swalWithBootstrapButtons({
                            title: 'Desea eliminar el registro?',
                            text: "Al eliminar esto no hay vuelta atras!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Si, eliminar!',
                            cancelButtonText: 'No, cancelar!',
                            reverseButtons: true
                        }).then((result) => {
                            //Acepto eliminar el registro
                            if (result.value) {
                                fetch('/roles/' + id + '/destroy', {
                                    method: 'delete',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                }).then(response =>
                                    response.json().then(json => {
                                        sweetAlert('Eliminado', json.message, 'success');
                                        setTimeout(() => {
                                            window.location.reload(true)
                                        }, 2000);
                                    })
                                );
                            } else if (
                                result.dismiss === swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons(
                                    'Cancelada',
                                    'La operacion a sido cancelada',

                                    'error'
                                )
                            }
                        })
         }
    </script>
@endpush