@extends('layouts.app')

@section('content')

    <div class="content-body">
        <!-- Input Validation start -->
    <section class="input-validation">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Editar entidad bancaria</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                            
                                @include('modulos.type_predios._form')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Input Validation end -->
    </div>
    
@endsection

@push('scripts')
  
@endpush