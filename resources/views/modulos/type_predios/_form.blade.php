@isset($type_predio)
{!! Form::model($type_predio, ['route' => ['predios.update', $type_predio->id], 'class' => 'form-horizontal']) !!}
{{ method_field('PATCH') }}
@endisset

@empty($type_predio)
    {!! Form::open(['route' => 'predios.store', 'class' => 'form-horizontal']) !!}
@endempty
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Nombre</label>
                <div class="controls">
                    <input type="text" name="nombre" class="form-control" data-validation-required-message="Este campo es requerido" placeholder="Nombre" value="@isset($type_predio){{ $type_predio->nombre }}@endisset @empty($user){{ old('nombre') }}@endempty">
                    @error('nombre')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
{!! Form::close() !!}