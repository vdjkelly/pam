@extends('layouts.app')

@section('content')
<section class="row flexbox-container">
        <div class="col-xl-12 col-11 d-flex justify-content-center">
            <div class="card rounded-0 mb-0 w-75" style="box-shadow: 0px 0 14px rgba(0, 0, 0, 0.37)!important;">
                <div class="row m-0">
                    <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                        <img  style="width: 160px"  class="img-fluid" src="{{ asset('app-assets/images/logo/PAM.png') }}" alt="branding logo">
                        <p class="text-center text-bold-600 font-medium-2" style="color: black">Portal Digital de Aval&uacute;os de Moravain</p>
                        <p class="text-center text-bold-600 mb-0" style="font-size: 15px; color: black">MORAVAIN &copy; 2020</p>
                        <p class="text-center text-bold-600" style="font-size: 12px; color: black">
                            <a href="https://estandartecnologia.com/" target="_blank" style="color: black">Portal Web Desarrollado por Estándar Tecnología</a>
                        </p>
                    </div>
                    <div class="col-lg-6 col-12 p-0">
                        <div class="card rounded-0 mb-0 px-2">
                            <div class="card-header pb-1">
                                <div class="card-title">
                                    <h4 class="mb-0">{{ __('Login') }}</h4>
                                </div>
                            </div>
                            <p class="px-2">Bienvenido a PAM.</p>
                            <div class="card-content">
                                <div class="card-body pt-1">
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <fieldset class="form-label-group form-group position-relative has-icon-left">
                                            <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="user-name" placeholder="Email" required>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <div class="form-control-position">
                                                <i class="feather icon-user"></i>
                                            </div>
                                            <label for="user-name">Email</label>
                                        </fieldset>

                                        <fieldset class="form-label-group position-relative has-icon-left">
                                            <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="user-password" placeholder="Password" required>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <div class="form-control-position">
                                                <i class="feather icon-lock"></i>
                                            </div>
                                            <label for="user-password">Password</label>
                                        </fieldset>
                                        <div class="form-group d-flex justify-content-between align-items-center">

                                        </div>

                                        <button type="submit" class="btn btn-primary float-right btn-inline mb-3">{{ __('Login') }}</button>
                                    </form>
                                </div>
                            </div>
                            <div class="login-footer">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
