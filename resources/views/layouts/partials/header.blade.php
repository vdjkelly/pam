<!-- BEGIN: Header-->
<div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
            <div class="navbar-wrapper">
                <div class="navbar-container content">
                    <div class="navbar-collapse" id="navbar-mobile">
                        <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                            <ul class="nav navbar-nav">
                                <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                            </ul>
                            <ul class="nav navbar-nav bookmark-icons">

                            </ul>
                            <ul class="nav navbar-nav">
                            {{-- <li>{{ auth()->user()->notifications->count() }}</li> --}}

                            </ul>
                        </div>
                        <ul class="nav navbar-nav float-right">
                            @role('Avaluador')
                            <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon feather icon-bell"></i><span class="badge badge-pill badge-primary badge-up">{{ auth()->user()->unreadNotifications->count() }}</span></a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <div class="dropdown-header m-0 p-2">
                                        <h3 class="white">{{ auth()->user()->unreadNotifications->count() }}</h3><span class="notification-title">@if (auth()->user()->unreadNotifications->count() > 1)Notificaciones @else Notificación @endif
                                        </span>
                                        </div>
                                    </li>
                                    <li class="scrollable-container media-list">
                                            @foreach(auth()->user()->unreadNotifications()->take(4)->get() as $notification)
                                                @isset($notification->data['failed'])
                                                    <a class="d-flex justify-content-between" href="{{ route('casos.show', ['id' =>  $notification->data['caso_id'], 'notification' => $notification->id  ])}}">
                                                        <div class="media d-flex align-items-start">
                                                            <div class="media-left"><i class="feather icon-plus-square font-medium-5 primary"></i></div>
                                                            <div class="media-body">
                                                                <h6 class="primary media-heading">{{ $notification->data['failed'] }}</h6>
                                                                <br>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endisset
                                                @empty($notification->data['failed'])
                                                <a class="d-flex justify-content-between" href="{{ route('casos.show', ['id' =>  $notification->data['caso_id'], 'notification' => $notification->id  ])}}">
                                                    <div class="media d-flex align-items-start">
                                                        <div class="media-left"><i class="feather icon-plus-square font-medium-5 primary"></i></div>
                                                        <div class="media-body">
                                                        <h6 class="primary media-heading">Avaluo # {{ $notification->data['numero_avaluo'] }}</h6><small class="notification-text">Fecha visita {{ $notification->data['fecha_visita'] }}</small>
                                                        <br>
                                                        <small class="notification-text">Entrega informe: {{ $notification->data['fecha_entrega_informe'] }} </small>
                                                        </div><small>
                                                            <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">{{ $notification->created_at->diffForHumans() }}</time></small>
                                                    </div>
                                                </a>
                                                @endempty
                                            @endforeach
                                    </li>
                                    <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" href="javascript:void(0)">Ver todas</a></li>
                                </ul>
                            </li>
                            @endrole

                            @role('Analista de Avaluos')
                            <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="ficon feather icon-bell"></i><span class="badge badge-pill badge-primary badge-up">{{ auth()->user()->unreadNotifications->count() }}</span></a>
                                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                    <li class="dropdown-menu-header">
                                        <div class="dropdown-header m-0 p-2">
                                            <h3 class="white">{{ auth()->user()->unreadNotifications->count() }}</h3><span class="notification-title">@if (auth()->user()->unreadNotifications->count() > 1)Notificaciones @else Notificación @endif
                                        </span>
                                        </div>
                                    </li>
                                    <li class="scrollable-container media-list">
                                        @foreach(auth()->user()->unreadNotifications()->take(4)->get() as $notification)
                                            @isset($notification->data['text'])
                                                <a class="d-flex justify-content-between" href="{{ route('revisiones.show', ['id' =>  $notification->data['caso_id']])}}">
                                                    <div class="media d-flex align-items-start">
                                                        <div class="media-left"><i class="feather icon-plus-square font-medium-5 primary"></i></div>
                                                        <div class="media-body">
                                                            <h6 class="primary media-heading">{{ $notification->data['text'] }}</h6>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </a>
                                            @endisset
                                        @endforeach
                                    </li>
                                    <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center" href="javascript:void(0)">Ver todas</a></li>
                                </ul>
                            </li>
                            @endrole

                            <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600"> {{ auth()->user()->name }} </span><span class="user-status"></span></div><span><img class="round" src="{{ isset(auth()->user()->avatar) ? asset(auth()->user()->avatar)   : asset('app-assets/images/portrait/small/avatar-s-11.png') }}" alt="avatar" height="40" width="40" /></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{ route('usuarios.show', ['id' => auth()->user()->id]) }}"><i class="feather icon-user"></i> Editar cuenta</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();"><i class="feather icon-power"></i> Salir</a>
                                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- END: Header-->
