<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow expanded" data-scroll-to-active="true">
        <div class="navbar-header expanded">
            <span class="nav-item nav-toggle d-flex justify-content-end"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></span>

            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand">
                        <div class="brand-logo" style="width: 238px; margin-top: -30px;">
                            <img class="img-fluid" src="{{ asset('app-assets/images/logo/logotipo-moravain.png') }}" alt="">
                        </div>
                    </a></li>
            </ul>
        </div>
        <div class="shadow-bottom mt-2"></div>
        <div class="main-menu-content mt-2">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                @if (Auth::user()->hasAnyPermission(['casos', 'casos.getNotNewCreate', 'casos.create']) || Auth::user()->hasRole('Administrador'))
                    <li class="{{ request()->is('casos*') ? 'active' : '' }} nav-item"><a href="{{ route('casos') }}"><i class="feather icon-file-plus"></i><span class="menu-item" data-i18n="Analytics">Crear solicitud </span></a>
                </li>
                @endif

                @if(Auth::user()->hasRole('Cliente'))
                        <li class="{{ request()->is('cliente*') ? 'active' : '' }} nav-item"><a href="{{ route('casos.getAvaluosCliente') }}"><i class="fa fa-industry"></i><span class="menu-item" data-i18n="Analytics">Casos</span></a>
                        </li>
                @endif

                @can('entidad.index')
                <li class="{{ request()->is('entidades*') ? 'active' : '' }} nav-item"><a href="{{ route('entidades') }}"><i class="fa fa-industry"></i><span class="menu-item" data-i18n="Analytics">Entidades</span></a>
                </li>
                @endcan

                @can('predios.index')
                <li class="{{ request()->is('predios*') ? 'active' : '' }} nav-item"><a href="{{ route('predios') }}"><i class="fa fa-th"></i><span class="menu-item" data-i18n="Analytics">Tipo de predios</span></a>
                </li>
                @endcan


                @can('usuarios')
                <li class="{{ request()->is('usuarios*') ? 'active' : '' }} nav-item"><a href="{{ route('usuarios') }}"><i class="fa fa-users"></i><span class="menu-item" data-i18n="Analytics">Usuarios</span></a>
                </li>
                @endcan


                @can('roles')
                <li class="{{ request()->is('roles*') ? 'active' : '' }} nav-item"><a href="{{ route('roles') }}"><i class="fa fa-expeditedssl"></i><span class="menu-item" data-i18n="Analytics">Roles</span></a>
                </li>
                @endcan


                @can('tipos_avaluos')
                <li class="{{ request()->is('tipos-avaluos*') ? 'active' : '' }} nav-item"><a href="{{ route('tipos_avaluos') }}"><i class="fa fa-adn"></i><span class="menu-item" data-i18n="Analytics">Tipo Avaluos</span></a>
                </li>
                @endcan

                    @can('consultas')
                        <li class="{{ request()->is('consultas*') ? 'active' : '' }} nav-item"><a href="{{ route('consultas') }}"><i class="fa fa-expeditedssl"></i><span class="menu-item" data-i18n="Analytics">Consultas avaluos</span></a>
                        </li>
                    @endcan

                    @if(Auth::user()->hasRole('Entidad'))
                        <li class="{{ request()->is('entidad*') ? 'active' : '' }} nav-item"><a href="{{ route('GetAvaluosEntidades') }}"><i class="fa fa-expeditedssl"></i><span class="menu-item" data-i18n="Analytics">Consultas avaluos</span></a>
                        </li>
                    @endif


                @if (Auth::user()->hasAnyPermission(['casos.show']) || Auth::user()->hasRole('Administrador'))
                <li class="{{ request()->is('gestion*') ? 'active' : '' }} nav-item"><a href="{{ route('gestion.index') }}"><i class="feather icon-clock"></i><span class="menu-item" data-i18n="Analytics">Todos los casos</span></a>
                </li>
                @endif

                @if (Auth::user()->hasAnyPermission(['revisor.index']) || Auth::user()->hasRole('Administrador'))
                <li class="{{ request()->is('revisor*') ? 'active' : '' }} nav-item"><a href="{{ route('revisor.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Revision de casos</span></a>
                </li>
                @endif


                    @if (Auth::user()->hasAnyPermission(['revisiones', 'envios']) || Auth::user()->hasRole('Administrador'))
                        <li class="{{ request()->is('revision*') ? 'active' : '' }} nav-item"><a href="{{ route('revisiones') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Rev. o envio de casos</span></a>
                        </li>
                    @endif
                    @can('observaciones')
                    <li class="{{ request()->is('observaciones*') ? 'active' : '' }} nav-item"><a href="{{ route('observaciones.index') }}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Observacion de casos</span></a>
                    </li>
                    @endcan


                @can('facturaciones')
                    <li class="{{ request()->is('facturaciones*') ? 'active' : '' }} nav-item"><a href="{{ route('facturaciones') }}"><i class="feather icon-clipboard"></i><span class="menu-item" data-i18n="Analytics">Facturaciones</span></a>
                    </li>
                @endcan
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->
