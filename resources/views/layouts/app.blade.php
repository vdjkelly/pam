<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Moravain - Avalúos, inmoviliaria & construcción') }}</title>

    <link rel="apple-touch-icon" href="{{ asset('app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/themes/semi-dark-layout.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/authentication.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/validation/form-validation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/notify/css/jquery.growl.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/notify/css/notifIt.css') }}">
    <link href="{{asset('app-assets/sweet-alert/sweetalert.css')}}" rel="stylesheet" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!-- END: Custom CSS-->

    @stack('styles')
    <style media="screen">
        .swal2-confirm {
            margin-left: 8px;
        }
    </style>
    @auth
        <script>
            window.BASE_URL = "{{ asset('') }}";
            window.PUSHER_APP_KEY = "{{ env('PUSHER_APP_KEY') }}";
            window.PUSHER_APP_CLUSTER = "{{ env('PUSHER_APP_CLUSTER') }}";
            window.NOTIFICATION_TOTAL = "{{ auth()->user()->unreadNotifications->count() }}";
        </script>
    @endauth
    

</head>


@guest
    <body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  menu-collapsed blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
@endguest

@auth
<body class="vertical-layout vertical-menu-modern content-left-sidebar chat-application navbar-floating footer-static  menu-expanded pace-done" data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">
@endauth
    <div id="app">
        @auth
            @include('layouts.partials.menu')
        @endauth
        
        <div class="app-content content">

            @auth
                @include('layouts.partials.header')
            @endauth
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/components.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/validation/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/validation/form-validation.js') }}"></script>
    <script src="{{ asset('app-assets/notify/js/jquery.growl.js') }}"></script>
    <script src="{{ asset('app-assets/notify/js/notifIt.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <!-- Sweet alert Plugin -->
    <script src="{{asset('app-assets/sweet-alert/sweetalert.min.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script src="{{asset('assets/js/scripts.js')}}"></script>
    <!-- END: Theme JS-->

    @if(Session::has('message'))
    <script type="text/javascript">
      $(document).ready(function () {
        notif({
                  type: "success",
                  msg: "{{Session::get('message')}}",
                  position: "center",
                  width: 500,
                  height: 60,
                  autohide: false
        });
      });
      </script>
  @endif

  

    <!-- BEGIN: Page JS-->
        @stack('scripts')
    <!-- END: Page JS-->
   
</body>
</html>
