<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileAvaluoSolicitud extends Model
{
    protected $fillable = ['avaluo_solicitud_id', 'file_certificacion', 'file_identificacion', 'file_soporte', 'file_escritura'];

    public function avaluo_solicitud()
    {
        return $this->belongsTo(AvaluoSolicitud::class);
    }
}
