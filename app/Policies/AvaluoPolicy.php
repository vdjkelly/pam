<?php

namespace App\Policies;

use App\Avaluo;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AvaluoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any avaluos.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the avaluo.
     *
     * @param  \App\User  $user
     * @param  \App\Avaluo  $avaluo
     * @return mixed
     */
    public function view(User $user, Avaluo $avaluo)
    {
        //
    }

    /**
     * Determine whether the user can create avaluos.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->can(['casos.getNotNewCreate', 'revisor.getGestionRevisor'])) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the avaluo.
     *
     * @param  \App\User  $user
     * @param  \App\Avaluo  $avaluo
     * @return mixed
     */
    public function update(User $user, Avaluo $avaluo)
    {
        //
    }

    /**
     * Determine whether the user can delete the avaluo.
     *
     * @param  \App\User  $user
     * @param  \App\Avaluo  $avaluo
     * @return mixed
     */
    public function delete(User $user, Avaluo $avaluo)
    {
        //
    }

    /**
     * Determine whether the user can restore the avaluo.
     *
     * @param  \App\User  $user
     * @param  \App\Avaluo  $avaluo
     * @return mixed
     */
    public function restore(User $user, Avaluo $avaluo)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the avaluo.
     *
     * @param  \App\User  $user
     * @param  \App\Avaluo  $avaluo
     * @return mixed
     */
    public function forceDelete(User $user, Avaluo $avaluo)
    {
        //
    }
}
