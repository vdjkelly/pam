<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    protected $fillable = ['nombre', 'user_id'];

    public function entidad()
    {
        return $this->hasMany(Avaluo::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
