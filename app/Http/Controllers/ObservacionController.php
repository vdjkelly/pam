<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Avaluo;
use App\AvaluoObservacion;
use App\Notifications\NotificationComment;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Notification;
use Spatie\Permission\Models\Role;

class ObservacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // dd(auth()->user()->getRoleName() == 'Avaluador');
        if(auth()->user()->getRoleName() == 'Avaluador' || auth()->user()->getRoleName() == 'Entidad') {
            $avaluos = Avaluo::where('avaluador_id', auth()->user()->id)->get();
        } else {
            $avaluos = Avaluo::get();
        }
        
        return view("modulos.observaciones.index", compact("avaluos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'observacion' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $avaluo = Avaluo::with(['cliente'])->findOrFail($request->input('avaluo_id'));

            $avaluoObservacion = AvaluoObservacion::create([
                'user_id' => auth()->user()->id,
                'avaluo_id' => $avaluo->id,
                'observacion' => $request->observacion
            ]);

            //Analista
            if (auth()->user()->id != $avaluo->user_id &&  $avaluo->avaluador_id != null) {
                $rolAnalista = User::find($avaluo->user_id);
                Notification::send($rolAnalista, (new NotificationComment($avaluo))->onQueue('email'));
            }

            if (auth()->user()->id != $avaluo->avaluador_id &&  $avaluo->avaluador_id != null) {
                $rolAvaluadores = User::find($avaluo->avaluador_id);
                Notification::send($rolAvaluadores, (new NotificationComment($avaluo))->onQueue('email'));
            }

            if (auth()->user()->id != $avaluo->revisor_tecnico_id &&  $avaluo->revisor_tecnico_id != null) {
                $rolRevisores = User::find($avaluo->revisor_tecnico_id);
                Notification::send($rolRevisores, (new NotificationComment($avaluo))->onQueue('email'));
            }

            if($request->has('observacion_file')) {
                $insert = [];
                foreach ($request->file('observacion_file') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_soporte = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_soporte);
                    $insert[]['observacion_file'] = "archivos/$file_soporte";
                }
                foreach ($insert as $file) {
                    $avaluoObservacion->observacion_file()->create($file);
                }

            }



            DB::commit();
            Session::flash('message','La observacion se realizo exitosamente');
            return redirect()->route('observaciones.index');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $avaluo = Avaluo::with(['avaluao_observacion'])->findOrFail($id);
        return view("modulos.observaciones.show", compact("avaluo"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
