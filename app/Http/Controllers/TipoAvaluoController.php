<?php

namespace App\Http\Controllers;

use App\TipoAvaluo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TipoAvaluoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avaluos = TipoAvaluo::get();
        return view('modulos.tipo_avaluos.index', compact('avaluos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('modulos.tipo_avaluos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|unique:tipo_avaluos,nombre',
        ]);

        TipoAvaluo::create(['nombre' => $request->input('nombre')]);
        Session::flash('message','Los datos se han creado exitosamente.');
        return redirect()->route('tipos_avaluos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $avaluo = TipoAvaluo::find($id);
        return view('modulos.tipo_avaluos.edit', compact('avaluo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
        ]);

        

        $avaluo = TipoAvaluo::find($id);
        $avaluo->nombre = $request->input('nombre');
        $avaluo->save();

        Session::flash('message','Los datos se han editado exitosamente.');
        return redirect()->route('tipos_avaluos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $avaluo = TipoAvaluo::findOrFail($id);

        if ($avaluo->delete()) {
            $response = [
             'id'        =>  $avaluo->id,
             'status'    =>  'success',
             'message'   =>  'Registro eliminado',
         ];
        } else {
            $response = [
             'status'    =>  'error',
             'message'   =>  'Intente nuevamente'
         ];
        }
       
        return response()->json($response);
    }
}
