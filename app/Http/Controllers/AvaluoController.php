<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Avaluo;
use App\User;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class AvaluoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avaluos = Avaluo::orderBy('numero_avaluo', 'desc')->get();
        return view('modulos.revision.index', compact('avaluos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $avaluo = Avaluo::find($id);

        $revisores = User::whereHas('roles', function ($query) {
            $query->where('name', 'Revisor Técnico');
        })->get();

        DB::table('notifications')
            ->where('data->caso_id',  $avaluo->id)
            ->update(['read_at' => Date::now()]);


        return view('modulos.revision.show', compact('avaluo', 'revisores'));
    }


    public function sendAvaluo($id)
    {
        $avaluo = Avaluo::find($id);
        
        return view('modulos.revision.send', compact('avaluo'));
    }

    public function getConsulta(Request $request)
    {
        $avaluos = Avaluo::orderBy('numero_avaluo', 'desc')->get();
        //dd($avaluos);
        return view('modulos.consultas.index', compact('avaluos'));
    }


    public function getAvaluoDetalles($id)
    {
        $avaluo = Avaluo::findOrFail($id);
        return view('modulos.consultas.detalles', compact('avaluo'));
    }


    public function getEntidadAvaluo()
    {
        if (auth()->user()->entidad->id == null){
            abort(403, "El usuario no es una entidad, o no se realaciona con una de ellas.!");
        }
        $avaluos = Avaluo::where('entidad_id', auth()->user()->entidad->id)->get();
        //dd($avaluos);
        return view('modulos.entidades.avaluos', compact('avaluos'));
    }

    public function getAvaluoShow($id)
    {
        $avaluo = Avaluo::findOrFail($id);
        return view('modulos.entidades.show_caso', compact('avaluo'));
    }

}
