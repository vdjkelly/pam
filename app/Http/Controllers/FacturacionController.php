<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facturacion;
use App\Avaluo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class FacturacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $this->authorize('facturaciones', Avaluo::class);

        $avaluos = Avaluo::with(['user', 'cliente.facturacion'])->where('avaluo', true)->get();

        return view('modulos.facturacion.index', compact('avaluos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $avaluo  = Avaluo::findOrFail($id);
        return view('modulos.facturacion.send', compact('avaluo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'avaluo_id' => 'required',
                'facturado' => 'required'
            ]
        );

        $avaluo = Avaluo::findOrFail($request->input('avaluo_id'));
        $avaluo->facturado = true;
        $avaluo->fecha_facturacion =  Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
        $avaluo->save();

        Session::flash('message','Proceso de avaluo facturado exitosamente.');
        return redirect()->route('facturaciones');
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       // $this->authorize('facturaciones.show', Facturacion::class);

        $facturacion = Facturacion::with(['avaluo.avaluo_solicitud.file_avaluo_solicitud'])->findOrFail($id);

        return view('modulos.facturacion.show', compact('facturacion'));
    }

    
}
