<?php

namespace App\Http\Controllers;

use App\Avaluo;
use App\User;
use Illuminate\Http\Request;
use App\Entidad;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class EntidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('entidad.index', Entidad::class);
        $entidades = Entidad::get();
        return view('modulos.entidades.index', compact('entidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('entidad.create', Entidad::class);
        return view('modulos.entidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('entidad.create', Entidad::class);

       // dd($request->all());
        $this->validator($request->all())->validate();


        $user = User::create([
            'name' => $request->nombre,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $user->assignRole('Entidad');

        Entidad::create([
            'nombre' => $request->input('nombre'),
            'user_id' =>  $user->id
        ]);

        Session::flash('message','Los datos se han creado exitosamente.');
        return redirect()->route('entidades');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('entidad.update', Entidad::class);
        $entidad = Entidad::findOrFail($id);
        return view('modulos.entidades.edit', compact('entidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('entidad.update', Entidad::class);
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $entidad = Entidad::findOrFail($id);

        $entidad->nombre = $request->input('nombre');
        $entidad->save();

        $user = User::findOrFail($entidad->user_id);
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->telefono = $request->telefono;
        $user->celular = $request->celular;
        $user->nit = $request->nit;
        if($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        Session::flash('message','Los datos se han editado exitosamente.');
        return redirect()->route('entidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $entidad = Entidad::findOrFail($id);
        $this->authorize('entidad.destroy', $entidad);
        if ($entidad->delete()) {
            $response = [
             'id'        =>  $entidad->id,
             'status'    =>  'success',
             'message'   =>  'Registro eliminado',
         ];
        } else {
            $response = [
             'status'    =>  'error',
             'message'   =>  'Intente nuevamente'
         ];
        }

        return response()->json($response);
    }

    public function getAvaluoDetails($id)
    {
        $avaluo = Avaluo::where('entidad_id', auth()->user()->entidad->id)->findOrFail($id);
        
        return view("modulos.entidades.detalle_avaluo", compact('avaluo'));
    }
}
