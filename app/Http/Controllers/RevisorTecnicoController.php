<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Avaluo;
use App\User;
use App\AvaluoObservacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Notifications\AvaluoFailed;
use Carbon\Carbon;

class RevisorTecnicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avaluo = Avaluo::where('revisor_tecnico_id', auth()->user()->id)->orderBy('avaluos.numero_avaluo', 'asc')->get();
        return view('modulos.revisor.index', compact('avaluo'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'revisor_tecnico_id' => 'required',
            'observacion' => 'required'
        ]);

        try {
            DB::beginTransaction();
            $avaluo = Avaluo::findOrFail($request->input('avaluo_id'));

            $avaluo->revisor_tecnico_id = $request->revisor_tecnico_id;
            $avaluo->fecha_entrega_cliente = $request->fecha_entrega_cliente;

            /**
             * Asignamos estatus 10 En revision.
             */
            if ($avaluo->status == 2) {
                $avaluo->status = 10;
            }

            $avaluo->fecha_asignacion_revisor = Carbon::now();
            $avaluo->save();

            AvaluoObservacion::create([
                'user_id' => auth()->user()->id,
                'avaluo_id' => $avaluo->id,
                'observacion' => $request->observacion
            ]);



            DB::commit();
            Session::flash('message','Los datos se han creado exitosamente.');
            return redirect()->route('revisiones');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }


    public function getStoreRevisor(Request $request)
    {
        $this->validate($request, [
            'valor_comercial' => 'required',
            'observacion' => 'required'
        ]);


        $avaluo = Avaluo::findOrFail($request->input('avaluo_id'));

        if($avaluo->status == 3){
            Session::flash('message','Lo sentimos pero no se a realizado la visita a este caso.');
            return redirect()->route('revisor.index');
        }
        try {
            DB::beginTransaction();



            if($avaluo->valor_comercial != $request->input('valor_comercial')) {
                $avaluo->valor_comercial = $request->valor_comercial;
            }

            if($request->avaluo == 0 && $avaluo->status == 10) {
                $avaluo->status =  6;
                $avaluo->avaluo == $request->avaluo;
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
                $user = User::find($avaluo->avaluador_id);
                $user->notify(new AvaluoFailed($avaluo));
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            }


            if($request->avaluo == 0 && $avaluo->status == 7) {
                $avaluo->status =  6;
                $avaluo->avaluo == $request->avaluo;
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
                $user = User::find($avaluo->avaluador_id);
                $user->notify(new AvaluoFailed($avaluo));
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            }

            if($request->avaluo == 1 && $avaluo->status == 10) {
                $avaluo->avaluo = $request->avaluo;
                $avaluo->status =  5;
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            }

            if($request->avaluo == 1 && $avaluo->status == 7) {
                $avaluo->avaluo = $request->avaluo;
                $avaluo->status =  5;
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            }



            if($request->avaluo == 1 && $avaluo->status == 9) {
                $avaluo->avaluo = $request->avaluo;
                $avaluo->status =  5;
                $avaluo->fecha_hora_revision = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            }


            if ($request->has('informe')) {
                $avaluo->file_avaluo()->delete();
                foreach ($request->file('informe') as $files) {
                $destinationPath = 'archivos/'; // upload path
                $informes = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $informes);
                $insert[]['informe'] = "archivos/$informes";
                }

                foreach ($insert as $file) {
                    $avaluo->file_avaluo()->create($file);
                }
            }
            
            $avaluo->save();

            AvaluoObservacion::create([
                'user_id' => auth()->user()->id,
                'avaluo_id' => $avaluo->id,
                'observacion' => $request->observacion
            ]);


            DB::commit();
            Session::flash('message','Los datos se han creado exitosamente.');
            return redirect()->route('revisor.index');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $avaluo = Avaluo::with(['user', 'avaluo_solicitud', 'entidad', 'avaluao_observacion.user'])->findOrFail($id);
        $revisores_tecnico = User::whereHas('roles', function ($query) {
            $query->where('name', 'Revisor Técnico');
        })->get();
        return view('modulos.revisor.show', compact('avaluo', 'revisores_tecnico'));
    }

    public function getGestionRevisor($id)
    {
        $avaluo = Avaluo::with(['user', 'avaluo_solicitud', 'entidad', 'avaluao_observacion.user', 'file_avaluo'])->findOrFail($id);

        return view('modulos.revisor.revisor', compact('avaluo'));
    }

}
