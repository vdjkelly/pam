<?php

namespace App\Http\Controllers;

use App\Notifications\AvaluoFailed;
use App\Notifications\NotificationAnalista;
use App\User;
use Illuminate\Http\Request;
use App\Avaluo;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\AvaluoObservacion;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

class GestionAvaluadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avaluos = DB::table('avaluos')
                        ->join('notifications', function ($join) {
                            $join->on('avaluos.id', '=', 'notifications.data->caso_id')
                                ->where('notifications.notifiable_id', '=',  auth()->user()->id)
                                ->whereNull('read_at');
                        })
                        ->join('clientes', function ($join) {
                            $join->on('avaluos.cliente_id', '=', 'clientes.id');
                        })
                        ->select('avaluos.*', 'notifications.id as notification_id', 'clientes.first_name', 'clientes.last_name')
                        ->get();
        return view('modulos.gestion.avaluador', compact('avaluos'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->visita === 0) {
            $this->validate($request, [
                'visita' => 'required',
                'avaluo_id' => 'required',
                'notification_id' => 'required',
                'informe.*' => 'max:2048',
                'valor_comercial' => 'required'
            ]);
        }

        try {
            DB::beginTransaction();
            $avaluo = Avaluo::findOrFail($request->input('avaluo_id'));
            //$avaluo->valor_comercial = isset($request->valor_comercial) ? $request->valor_comercial : $avaluo->valor_comercial;

            //Si no realiza la visita y el estatus es 1 pasamos a estatus 3 (Visita no realizada)
            if($request->visita == 0 && $avaluo->status == 1) {
                $this->validate($request, [
                    'visita' => 'required',
                    'avaluo_id' => 'required',
                    'notification_id' => 'required',
                ]);
                $avaluo->status = 3;

                $avaluo->avaluador_id = null;
                $avaluo->save();
            }
            //Si realiza la visita y el estatus es 1 pasamos a estatus 2 (Visita realizada)
            if($request->visita == 1 && $avaluo->status == 1) {
                $this->validate($request, [
                    'visita' => 'required',
                    'avaluo_id' => 'required',
                    'notification_id' => 'required',
                ]);
                $avaluo->fecha_entrega_avaluador = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
                $avaluo->status = 2;
                $avaluo->valor_comercial = $request->valor_comercial;
                $avaluo->visita = 1;
               // $avaluo->avaluador_id = null;
                $avaluo->save();

                $rol = Role::where("name", "Analista de Avaluos")->first();
                $analistas = User::whereHas("roles", function ($q) use ($rol) {
                    $q->where("roles.id", $rol->id);
                })->get();
                Notification::send($analistas, (new NotificationAnalista($avaluo)));
            }

            //Si realiza la visita y el estatus es 3 pasamos a estatus  2 (Visita realizada)
            if($request->visita == 1 && $avaluo->status == 3) {
                $this->validate($request, [
                    'visita' => 'required',
                    'avaluo_id' => 'required',
                    'notification_id' => 'required',
                ]);
                $avaluo->visita = $request->visita;
                $avaluo->valor_comercial = $request->valor_comercial;
                $avaluo->status = 2;
                $avaluo->save();

                $rol = Role::where("name", "Analista de Avaluos")->first();
                $analistas = User::whereHas("roles", function ($q) use ($rol) {
                    $q->where("roles.id", $rol->id);
                })->get();
                Notification::send($analistas, (new NotificationAnalista($avaluo)));
            }

            //Si realiza la visita y el estatus es 4 (Reasignacion de Avaluador) pasamos a estatus  2
            if($request->visita == 1 && $avaluo->status == 4) {
                $this->validate($request, [
                    'visita' => 'required',
                    'avaluo_id' => 'required',
                    'notification_id' => 'required',
                ]);

                $avaluo->valor_comercial = $request->valor_comercial;
                $avaluo->fecha_entrega_avaluador = Carbon::now();
                $avaluo->status = 2;
                $avaluo->save();

                $rol = Role::where("name", "Analista de Avaluos")->first();
                $analistas = User::whereHas("roles", function ($q) use ($rol) {
                    $q->where("roles.id", $rol->id);
                })->get();
                Notification::send($analistas, (new NotificationAnalista($avaluo)));
            }

            //Si el estatus 6 y se realizo la visita se hace la correccion de revision estatus 7 (Correccion para revision)
            if($avaluo->visita == 1 && $avaluo->status == 6) {
                $this->validate($request, [
                    'avaluo_id' => 'required',
                    'notification_id' => 'required',
                ]);
                $avaluo->valor_comercial = $request->valor_comercial;
                $avaluo->status = 7;
                $avaluo->save();
            }



            if($avaluo->visita == 0 && $avaluo->status == 6) {
                $this->validate($request, [
                    'avaluo_id' => 'required',
                    'notification_id' => 'required',
                ]);
                $avaluo->valor_comercial = $request->valor_comercial;
                $avaluo->status = 7;
                $avaluo->visita = 1;
                $avaluo->save();
            }


            if ($request->has('informe')) {
                $avaluo->file_avaluo()->delete();
                foreach ($request->file('informe') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $informes = date('YmdHis') . "." . $files->getClientOriginalExtension();
                    $files->move($destinationPath, $informes);
                    $insert[]['informe'] = "archivos/$informes";
                }

                foreach ($insert as $file) {
                    $avaluo->file_avaluo()->create($file);
                }
            }

            AvaluoObservacion::create([
                'user_id' => auth()->user()->id,
                'avaluo_id' => $avaluo->id,
                'observacion' => $request->observacion
            ]);


            $notification = $request->user()
                ->unreadNotifications()
                ->where('id', $request->input('notification_id'))
                ->first();

            if (is_null($notification)) {
                return response()->json('Notification not found.', 404);
            }

            $notification->delete();

            DB::commit();
            if($request->visita == false && $avaluo->status == 1) {
                Session::flash('message','Se ha cancelado el caso exitosamente.');
            } else {
                Session::flash('message','Los datos se han creado exitosamente.');
            }

            return redirect()->route('gestion.index');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('message', $e->getMessage());
            return redirect()->back();
        }


    }
}
