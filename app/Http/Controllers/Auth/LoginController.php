<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Redirect login
     */
    public function redirectTo()
    {
        $role = auth()->user()->getRoleId();
        //dd($role == 3);
        // Check user role
        if ($role == 3) {
            return '/casos';
        }
        if ($role == 4) {
            return '/gestion/avaluos';
        }
        if ($role == 5) {
            return '/revisor/avaluos';
        }
        if($role == 9) {
            return '/entidad/GetAvaluos';
        }
        if ($role != 5 && $role != 4 && $role != 3) {
            return '/home';
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);
        $this->authenticated($request, $this->guard()->user());
       return redirect($this->redirectTo());
    }
}
