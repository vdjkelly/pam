<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('usuarios', User::class);


        $users = User::has('roles')->get();
        $roles = Role::where('name', '!=', 'Cliente')->pluck('id')->all();
        $users = User::whereHas('roles', function ($query) use ($roles) {
            $query->whereIn('id', $roles);
        })->get();
        return view('modulos.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //$this->authorize('usuarios.create', User::class);
        $roles = Role::where('name', '!=', 'Entidad')->get();
        return view('modulos.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->authorize('usuarios.create', User::class);

        $this->validator($request->all())->validate();

        $user = User::create([
            'name' => $request->name,
            'last_name' =>$request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $user->assignRole($request->input('roles'));
        Session::flash('message','Los datos se han creado exitosamente.');
        return redirect()->route('usuarios');
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles' => 'required'
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail(auth()->user()->id);
        return view('modulos.users.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$this->authorize('usuarios.edit', User::class);
        $user = User::find($id);
        $roles = Role::where('name', '!=', 'Entidad')->get();
        $userRole = $user->roles->pluck('name','id');


        return view('modulos.users.edit', compact('roles', 'user', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // $this->authorize('usuarios.edit', User::class);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => 'required|string|email|max:255||unique:users,email,'.$id,
            'roles' => 'required'
        ]);

        $user = User::find($id);


        if($request->input('password') != null) {
            $user->password = bcrypt($request->password);
        }



        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->save();

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        Session::flash('message','Los datos se han editado exitosamente.');
        return redirect()->route('usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // $this->authorize('usuarios.destroy', User::class);
        $user = User::findOrFail($id);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        if ($user->delete()) {
            $response = [
             'id'        =>  $user->id,
             'status'    =>  'success',
             'message'   =>  'Registro eliminado',
         ];
        } else {
            $response = [
             'status'    =>  'error',
             'message'   =>  'Intente nuevamente'
         ];
        }

        return response()->json($response);
    }

    public function getUpdateInfo(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'avatar' => 'required'
        ]);

        $user = User::find(auth()->user()->id);
        $user->name = $request->name;
        $user->last_name = $request->last_name;

        $path_file_avatar = '';
        if($request->has('avatar')) {
            $path_file_avatar = Storage::disk('custom')->put('usuarios', $request->file('avatar'));
            $user->avatar = $path_file_avatar;
        }
        $user->save();

        Session::flash('message','Los datos se han editado exitosamente.');
        return redirect()->back();
    }
}
