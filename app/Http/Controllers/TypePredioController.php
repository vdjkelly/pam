<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TypePredio;
use Illuminate\Support\Facades\Session;

class TypePredioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('predios.index', TypePredio::class);
        $type_predios = TypePredio::get();
        return view('modulos.type_predios.index', compact('type_predios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modulos.type_predios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->authorize('predios.store', TypePredio::class);
        $this->validate($request, [
            'nombre' => 'required|unique:type_predios'
        ]);

        TypePredio::create([
            'nombre' => $request->input('nombre')
        ]);

        Session::flash('message','Los datos se han creado exitosamente.');
        return redirect()->route('predios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // $this->authorize('predios.edit', TypePredio::class);
        $type_predio = TypePredio::findOrFail($id);
        return view('modulos.type_predios.edit', compact('type_predio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // $this->authorize('predios.edit', TypePredio::class);
        $this->validate($request, [
            'nombre' => 'required|unique:type_predios,nombre,'.$id
        ]);

        $type_predio = TypePredio::findOrFail($id);

        $type_predio->nombre = $request->input('nombre');
        $type_predio->save();

        Session::flash('message','Los datos se han editado exitosamente.');
        return redirect()->route('predios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // $this->authorize('predios.destroy', TypePredio::class);
        $type_predio = TypePredio::findOrFail($id);

        if ($type_predio->delete()) {
            $response = [
             'id'        =>  $type_predio->id,
             'status'    =>  'success',
             'message'   =>  'Registro eliminado',
         ];
        } else {
            $response = [
             'status'    =>  'error',
             'message'   =>  'Intente nuevamente'
         ];
        }
       
        return response()->json($response);
    }
}
