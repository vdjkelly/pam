<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendAvaluoCliente;
use App\Avaluo;
use App\AvaluoObservacion;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class AvaluoSolicitudController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $avaluo  = Avaluo::findOrFail($id);
        return view('modulos.facturacion.send', compact('avaluo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'informe' => 'required'
        ]);


        $avaluo = Avaluo::with(['cliente'])->findOrFail($request->input('avaluo_id'));


        $path_file_informe = '';
        if ($files = $request->file('informe')) {
            $destinationPath = 'archivos/'; // upload path
            $informe = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $informe);

            //dd($informe);


            $avaluo->informe = "archivos/$informe";
            $avaluo->enviado = true;
            $avaluo->fecha_real_envio = Carbon::parse(Carbon::now())->format('Y-m-d H:i:s');
            $avaluo->aquien = $request->input('nombre');
            $avaluo->save();
         }
        


        AvaluoObservacion::create([
            'user_id' => auth()->user()->id,
            'avaluo_id' => $avaluo->id,
            'observacion' => $request->observacion
        ]);
        $recipients = explode(',', $request->input('email'));

        Mail::to($recipients)->send(new SendAvaluoCliente($avaluo, $avaluo->cliente, $request->input('nombre'),$request->observacion));


        Session::flash('message','Los datos se han creado exitosamente.');
        return redirect()->route('revisiones');
    }
}
