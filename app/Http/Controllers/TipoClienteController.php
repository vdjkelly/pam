<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoCliente;

class TipoClienteController extends Controller
{
    public function index()
    {
        return response()->json(TipoCliente::get());
    }
}
