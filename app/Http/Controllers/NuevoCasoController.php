<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\TipoAvaluo;
use Illuminate\Http\Request;
use App\Departament;
use App\TypePredio;
use App\User;
use App\Avaluo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\AvaluoSolicitud;
use App\Facturacion;
use App\Entidad;
use App\AvaluoObservacion;
use Illuminate\Support\Facades\Session;
use App\Notifications\NotificationCaso;
use Illuminate\Support\Facades\Notification;
use App\TipoCliente;
use App\Mail\AvaluoSend;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Jenssegers\Date\Date;

class NuevoCasoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modulos.casos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departament::get();
        $tipo_predios = TypePredio::get();
        $avaluo = Avaluo::get()->pluck('numero_avaluo');
        $tipo_clientes = TipoCliente::get();
        if($avaluo->isEmpty()) {
            $generate_id = 1855;
        } else {
            $generate_id = collect($avaluo)->last() + 1;
        }

        return view('modulos.casos.create', compact('departamentos', 'tipo_predios', 'generate_id', 'tipo_clientes'));
    }


    public function getNotNewCreate($id)
    {


        $departamentos = Departament::get();
        $tipo_predios = TypePredio::get();
        $avaluo = Avaluo::with(['avaluo_solicitud'])->findOrFail($id);
        $tipo_clientes = TipoCliente::get();

        $entidades = Entidad::get();
        $avaluadores = User::whereHas('roles', function ($query) {
            $query->where('name', 'Avaluador');
        })->get();
        $cliente = Cliente::findOrFail($avaluo->cliente_id);

        $tipo_avaluos = TipoAvaluo::get();
        return view('modulos.casos.not_new_create', compact('departamentos', 'tipo_predios', 'entidades', 'avaluadores', 'cliente', 'avaluo', 'tipo_clientes', 'tipo_avaluos'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'tipo_cliente_id' => 'required',
            'celular' => 'required',
            'email' => 'required',
            'direccion' => 'required'
        ]);
        try {
            DB::beginTransaction();
            $data = [
                'first_name' => $request->name,
                'last_name' => $request->last_name,
                'telefono' => $request->telefono,
                'identificacion' => $request->identificacion,
                'tipo_cliente_id' => $request->tipo_cliente_id,
                'nit' => isset($request->nit) ? $request->nit : null,
                'celular' => $request->celular,
                'email' => $request->email
            ];

            event(new Registered($cliente = Cliente::create($data)));

           // $cliente->assignRole('Cliente');

            $avaluo_solicitud = AvaluoSolicitud::create([
                'certificacion' => $request->certificacion,
                'identificacion' => $request->identificacion,
                'soporte' => $request->soporte,
                'escritura' => $request->escritura,
                'direccion' => $request->direccion
            ]);

            if($request->check == 1 && $request->check_itaud == 0) {
                $facturacion = Facturacion::create([
                    'cliente_id' =>  $cliente->id,
                    'factura' => $request->name,
                    'telefono_factura' => $request->celular,
                    'email_factura' => $request->email,
                    'direccion' => $request->direccion,
                    'identificacion_factura' => $request->identificacion,
                    'banco_consigno' => $request->banco_consigno,
                    'valor_anticipo' => $request->valor_anticipo
                ]);
            } elseif($request->check == 0 && $request->check_itaud == 0) {
                $facturacion = Facturacion::create([
                    'cliente_id' => $cliente->id,
                    'factura' => $request->factura,
                    'telefono_factura' => $request->telefono_factura,
                    'email_factura' => $request->email_factura,
                    'direccion' => $request->direccion,
                    'identificacion_factura' => $request->identificacion_factura,
                    'banco_consigno' => $request->banco_consigno,
                    'valor_anticipo' => $request->valor_anticipo
                ]);
            }

            if($request->check == 0 && $request->check_itaud == 1) {
                $facturacion = Facturacion::create([
                    'cliente_id' =>  $cliente->id,
                    'factura' => "Itaú Corbanca Colombia S.A",
                    'telefono_factura' => "5818181",
                    'email_factura' => "asesorhipotecario1@cibergestion.com.co",
                    'direccion' => "Calle 100 No. 7-25",
                    'identificacion_factura' => "890903937-0",
                    'banco_consigno' => 0,
                    'valor_anticipo' => 0
                ]);
            }

            $path_file_identificacion_factura = '';
            if($request->has('file_identificacion_factura')) {
                $path_file_identificacion_factura = Storage::disk('custom')->put('archivos', $request->file('file_identificacion_factura'));
                $facturacion->file_identificacion_factura = $path_file_identificacion_factura;
                $facturacion->save();
            }


            $path_file_anticipo = '';
            if($request->has('file_anticipo')) {
                $path_file_anticipo = Storage::disk('custom')->put('archivos', $request->file('file_anticipo'));
                $facturacion->file_anticipo = $path_file_anticipo;
                $facturacion->save();
            }

            $cliente->avaluo()->create([
                'cliente_id' => $cliente->id,
                'user_id' => auth()->user()->id,
                'numero_avaluo' => $request->numero_avaluo,
                'fecha_solicitud' => NULL,
                'avaluo_solicitud_id' => $avaluo_solicitud->id,
                'departament_id' => $request->departament_id,
                'city_id' => $request->city_id,
                'type_predio_id' => $request->type_predio_id,
                'facturacion_id' => $facturacion->id,
                'fecha_entrega_cliente' => NULL
            ]);



            $path_file_certificacion = '';
            if($request->has('file_certificacion')) {
                $insert = [];
                foreach ($request->file('file_certificacion') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_certificacion = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_certificacion);
                    $insert[]['file_certificacion'] = "archivos/$file_certificacion";
                }
                foreach ($insert as $file) {
                    $avaluo_solicitud->file_avaluo_solicitud()->create($file);
                }
            }

            $path_file_identificacion = '';
            if($request->has('file_identificacion')) {
                $insert = [];
                foreach ($request->file('file_identificacion') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_identificacion = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_identificacion);
                    $insert[]['file_identificacion'] = "archivos/$file_identificacion";
                }
                foreach ($insert as $file) {
                    $avaluo_solicitud->file_avaluo_solicitud()->create($file);
                }

            }

            $file_soporte = '';

            if($request->has('file_escritura')) {
                $insert = [];
                foreach ($request->file('file_escritura') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_soporte = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_soporte);
                    $insert[]['file_escritura'] = "archivos/$file_soporte";
                }
                foreach ($insert as $file) {
                    $avaluo_solicitud->file_avaluo_solicitud()->create($file);
                }

            }

            DB::commit();
            Session::flash('message','Los datos se han creado exitosamente.');
            return redirect()->route('casos');
        } catch (\Exception $e) {
            DB::rollBack();
            Session::flash('message', $e->getMessage());
            return redirect()->route('casos');
        }

    }


    public function getInfoSolicitante(Request $request)
    {
        // $this->validate($request, [
        //     'avaluador_id' => 'required',
        //     'fecha_visita' => 'required',
        //     'hora_visita' => 'required',
        //     'fecha_entrega_informe' => 'required',
        //     'entidad_id' => 'required'
        // ]);
       try {
        DB::beginTransaction();
        $mytime = Carbon::now();
           $avaluo = Avaluo::with(['entidad', 'cliente', 'facturacion'])->findOrFail($request->input('avaluo_id'));
//dd($request->fecha_entrega_informe ." " . $mytime->format('H:i:s'));
           $avaluo->entidad_id = $request->entidad_id;
           $avaluo->fecha_visita = $request->fecha_visita;
           $avaluo->hora_visita = $request->hora_visita;
           $avaluo->fecha_entrega_informe = $request->fecha_entrega_informe ." " . $mytime->format('H:i:s');
           $avaluo->avaluador_id = $request->avaluador_id;
           $avaluo->tipo_avaluo_id = $request->tipo_avaluo_id;

           if( $avaluo->status == 0) {
               $avaluo->status = 1;
           }
           if( $avaluo->status == 3) {
               $avaluo->status = 4;
           }
           if( $avaluo->status == 9) {
               $avaluo->status = 1;
           }


           $avaluo->save();

           

           $solicitud = AvaluoSolicitud::with(['file_avaluo_solicitud'])->findOrFail($avaluo->avaluo_solicitud_id);
           $solicitud->identificacion = $request->identificacion;
           // $solicitud->direccion = $request->direccion;
           $solicitud->nombre = $request->nombre;
           $solicitud->telefono = $request->telefono;
           $solicitud->celular = $request->celular;
           $solicitud->contacto = $request->contacto;
           $solicitud->telefono_contacto = $request->telefono_contacto;
           $solicitud->consecutivo_entidades = $request->consecutivo_entidades;
           $solicitud->correo = $request->correo;
           $solicitud->save();
           
           $user = User::find($request->avaluador_id);

           $entidad = Entidad::find($request->entidad_id);

           

        // $user->notify((new NotificationCaso($avaluo, $avaluo->avaluo_solicitud, $entidad, $avaluo->cliente, $avaluo->facturacion, $request->observacion, $solicitud->file_avaluo_solicitud))->delay(5000)->onQueue('avaluo.notifications'));

        try{

           $user->notify((new NotificationCaso($avaluo, $avaluo->avaluo_solicitud, $entidad, $avaluo->cliente, $avaluo->facturacion, $request->observacion, $solicitud->file_avaluo_solicitud))->delay(5000)->onQueue('avaluo.notifications'));
           
           }
        catch(\Exception $e){ // Using a generic exception
            
            dump('Mail not sent'.$e);
          
          }
        
        //Mail::to($user)->send(new AvaluoSend($avaluo, $avaluo->avaluo_solicitud, $avaluo->entidad));

        //Notification::route('mail', $user->email)->notify(new NotificationCaso($avaluo, $user->email));
        //Notification::route('mail', $user->email)->notify((new NotificationCaso($avaluo))->delay(5000)->onQueue('avaluo.notifications'));
       //Notification::send($user, new NotificationCaso($avaluo));
       
        AvaluoObservacion::create([
            'user_id' => auth()->user()->id,
            'avaluo_id' => $avaluo->id,
            'observacion' => $request->observacion
        ]);

        
        DB::commit();
        Session::flash('message','Los datos se han creado exitosamente.');
           return redirect()->route('casos');
       } catch (\Exception $e) {
        DB::rollBack();
        Session::flash('message', $e->getMessage());
           return redirect()->route('casos');
       }
    }


    private function operacion_fecha($fecha,$dias) {
        list ($dia,$mes,$ano)=explode("-",$fecha);
        if (!checkdate($mes,$dia,$ano)){return false;}
        $dia=$dia+$dias;
        $fecha=date( "d-m-Y", mktime(0,0,0,$mes,$dia,$ano) );
        return $fecha;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $avaluo = Avaluo::with(['cliente', 'user', 'avaluo_solicitud', 'entidad','city', 'type_predio'])->where('avaluador_id', auth()->user()->id)->findOrFail($id);
        $notification = $request->get('notification');

        if ($avaluo->status == 5 || $avaluo->status == 2 || $avaluo->status == 8 || $avaluo->status == 10) {
            abort(403, "El proceso esta prohibido, hasta cumplir su procedimiento.");
        }


        return view('modulos.gestion.show', compact('avaluo', 'notification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('modulos.casos.edit');
    }
}
