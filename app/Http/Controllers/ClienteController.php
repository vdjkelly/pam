<?php

namespace App\Http\Controllers;

use App\AvaluoObservacion;
use Illuminate\Http\Request;
use App\User;
use App\Avaluo;
use Illuminate\Support\Facades\Session;
use App\TipoCliente;
use App\Departament;
use App\TypePredio;
use App\AvaluoSolicitud;
use App\Facturacion;
use App\Cliente;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{

    public function getAvaluosCliente()
    {
        $avaluos = Avaluo::where('cliente_id', auth()->user()->id)->with(['cliente', 'facturacion', 'avaluo_solicitud'])->get();
        return view('modulos.clientes.entrada', compact('avaluos'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* $clientes = User::whereHas('roles', function ($query) {
            $query->where('name', 'Cliente');
        })->get();]*/

        $avaluos = Avaluo::orderBy('avaluos.numero_avaluo', 'asc')->with(['user'])->get();
        return view('modulos.clientes.index', compact('avaluos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $avaluo = Avaluo::findOrFail($id);
        return view('modulos.clientes.show_caso', compact('avaluo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $avaluo = Avaluo::with(['cliente'])->findOrFail($id);
        $tipo_clientes = TipoCliente::get();
        $departamentos = Departament::get();
        $tipo_predios = TypePredio::get();
        return view('modulos.clientes.edit_form', compact('avaluo', 'tipo_clientes', 'departamentos', 'tipo_predios'));
    }

    public function updateDatosCliente(Request $request, $id)
    {

        $avaluo = Avaluo::findOrFail($id);

        $avaluo_solicitud = AvaluoSolicitud::findOrFail($avaluo->avaluo_solicitud_id);

        $cliente = Cliente::findOrFail($request->input('cliente_id'));
        $facturacion  = Facturacion::findOrFail($avaluo->facturacion_id);

        $cliente->first_name = $request->input('first_name');
        $cliente->last_name = $request->input('last_name');
        $cliente->email = $request->input('email');
        $cliente->last_name = $request->input('last_name');
        $cliente->telefono = $request->input('telefono');
        $cliente->celular = $request->input('celular');
        $cliente->identificacion = $request->input('identificacion');
        $cliente->nit = $request->input('nit');
        $cliente->tipo_cliente_id = $request->input('tipo_cliente_id');
        $cliente->save();

        $avaluo->departament_id  = $request->input('departament_id');
        $avaluo->city_id  = $request->input('city_id');
        //$avaluo->direccion  = $request->input('direccion');
        $avaluo->type_predio_id  = $request->input('type_predio_id');

        $avaluo->save();

        $avaluo_solicitud->certificacion =  $request->input('certificacion');
        $avaluo_solicitud->identificacion = $request->input('identificacion');
        $avaluo_solicitud->direccion  = $request->input('direccion');
        $avaluo_solicitud->escritura  = $request->input('escritura');

        if($request->has('file_certificacion') || $request->has('file_identificacion') || $request->has('file_escritura')) {
            $avaluo_solicitud->file_avaluo_solicitud()->delete();
        }


        $path_file_certificacion = '';
            if($request->has('file_certificacion')) {
                $insert = [];
                foreach ($request->file('file_certificacion') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_certificacion = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_certificacion);
                    $insert[]['file_certificacion'] = "archivos/$file_certificacion";
                }

                foreach ($insert as $file) {
                    $avaluo_solicitud->file_avaluo_solicitud()->create($file);
                }
            }

            $path_file_identificacion = '';
            if($request->has('file_identificacion')) {
                $insert = [];
                foreach ($request->file('file_identificacion') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_identificacion = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_identificacion);
                    $insert[]['file_identificacion'] = "archivos/$file_identificacion";
                }
                foreach ($insert as $file) {
                    $avaluo_solicitud->file_avaluo_solicitud()->create($file);
                }

            }

            $file_soporte = '';

            if($request->has('file_escritura')) {
                $insert = [];
                foreach ($request->file('file_escritura') as $files) {
                    $destinationPath = 'archivos/'; // upload path
                    $file_soporte = date('Y-m-d') . "-" . str_replace("\x96", "-", $files->getClientOriginalName());
                    $files->move($destinationPath, $file_soporte);
                    $insert[]['file_escritura'] = "archivos/$file_soporte";
                }
                foreach ($insert as $file) {
                    $avaluo_solicitud->file_avaluo_solicitud()->create($file);
                }

            }

            $avaluo_solicitud->save();

            $facturacion->factura = $request->input('factura');
            $facturacion->telefono_factura = $request->input('telefono_factura');
            $facturacion->email_factura = $request->input('email_factura');
            $facturacion->direccion = $request->input('direccion_factura');
            $facturacion->identificacion_factura = $request->input('identificacion_factura');
            $facturacion->banco_consigno = $request->input('banco_consigno');
            $facturacion->valor_anticipo = $request->input('valor_anticipo');

            $path_file_identificacion_factura = '';
            if($request->has('file_identificacion_factura')) {
                $path_file_identificacion_factura = Storage::disk('custom')->put('archivos', $request->file('file_identificacion_factura'));
                $facturacion->file_identificacion_factura = $path_file_identificacion_factura;
            }

            $path_file_anticipo = '';
            if($request->has('file_anticipo')) {
                $path_file_anticipo = Storage::disk('custom')->put('archivos', $request->file('file_anticipo'));
                $facturacion->file_anticipo = $path_file_anticipo;
            }

            $facturacion->save();

        Session::flash('message','Se han actualizado los datos exitosamente.');
        return redirect()->route('consultas');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $avaluo = Avaluo::findOrFail($id);

        $avaluo->status = 9;
        $avaluo->save();

        AvaluoObservacion::create([
            'user_id' => auth()->user()->id,
            'avaluo_id' => $avaluo->id,
            'observacion' => $request->observacion
        ]);
        Session::flash('message','Se ha cancelado el proceso exitosamente.');
        return redirect()->route('GetAvaluosEntidades');
    }
}
