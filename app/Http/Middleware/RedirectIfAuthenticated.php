<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            $role = auth()->user()->getRoleName(); 
    
            // Check user role
            switch ($role) {
                case 'Analista de Avaluos':
                        return redirect('/casos');
                    break;
                case 'Avaluador':
                        return redirect('/gestion/avaluos');
                    break; 
                    case 'Revisor Técnico':
                        return redirect('/casos');
                    break; 
                default:
                        return redirect('/home');
                    break;
            }

            
        }

        return $next($request);
    }
}
