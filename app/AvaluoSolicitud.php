<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvaluoSolicitud extends Model
{
    protected $fillable = ['certificacion', 'identificacion',  'escritura', 'soporte', 'direccion', 'consecutivo_entidades', 'nombre', 'contacto', 'telefono_contacto', 'telefono', 'celular', 'correo'];

    public function file_avaluo_solicitud()
    {
        return $this->hasMany(FileAvaluoSolicitud::class);
    }
}
