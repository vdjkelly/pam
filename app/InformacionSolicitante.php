<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacionSolicitante extends Model
{
    protected $fillable = ['entidad'];
}
