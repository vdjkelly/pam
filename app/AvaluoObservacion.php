<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AvaluoObservacion extends Model
{
    protected $fillable = ['user_id', 'avaluo_id', 'observacion'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function observacion_file() : HasMany
    {
        return $this->hasMany(ObservacionFile::class);
    }
}
