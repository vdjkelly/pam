<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'telefono',
        'celular',
        'tipo_cliente_id',
        'identificacion',
        'nit',
        'email'
    ];

    public function avaluo()
    {
        return $this->hasMany(Avaluo::class);
    }

    public function facturacion()
    {
        return $this->hasMany(Facturacion::class);
    }
}
