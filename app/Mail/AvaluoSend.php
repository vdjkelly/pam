<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AvaluoSend extends Mailable
{
    use Queueable, SerializesModels;

    protected $caso;

    protected $avaluo_solicitud;

    protected $entidad;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso, $avaluo_solicitud, $entidad)
    {
        $this->caso = $caso;
        $this->avaluo_solicitud = $avaluo_solicitud;
        $this->entidad = $entidad;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = url('/casos/'.$this->caso->id. '/show');
        $file_soporte = public_path($this->avaluo_solicitud->file_soporte);
        $orden_avaluo = public_path('archivos/A-1420.xlsx');
        $attachments = [
            $file_certificacion,
            $file_identificacion,
            $file_soporte,
            $orden_avaluo
        ];

        return $this->markdown('mail.casos.avaluador', ['url' => $url, 'caso' => $this->caso, 'entidad' => $this->entidad, 'solicitud' => $this->avaluo_solicitud])
                        ->subject('Tienes un nuevo caso por atender bajo el numero '. ' #'.  $this->caso->numero_avaluo)
                        ->attach(public_path($this->avaluo_solicitud->file_soporte))
                        ->attach(public_path('archivos/A-1420.xlsx'));

        foreach($attachments as $filePath){
            $email->attach($filePath);
        }
    }
}
