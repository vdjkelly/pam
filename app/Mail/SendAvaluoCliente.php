<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Avaluo;

class SendAvaluoCliente extends Mailable
{
    use Queueable, SerializesModels;

    public $avaluo;

    public $cliente;

    public $observacion;
    public $nombre;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Avaluo $avaluo, $cliente, $nombre, $observacion)
    {
        $this->avaluo = $avaluo;
        $this->cliente = $cliente;
        $this->observacion = $observacion;
        $this->nombre = $nombre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd(public_path($this->avaluo->informe));
        $subject = "Informe de avaluo " . "#". $this->avaluo->numero_avaluo . " " .$this->cliente->first_name . " " . $this->cliente->last_name;
        return $this->markdown('mail.send.avaluo_cliente')
                ->subject($subject)
                ->with([
                    'numero_avaluo' => $this->avaluo->numero_avaluo,
                    'nombre' => $this->nombre,
                    'observacion' => $this->observacion,
                ])
                ->attach(public_path($this->avaluo->informe));
    }
}
