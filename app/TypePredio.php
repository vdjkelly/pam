<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePredio extends Model
{
    protected $fillable = ['nombre'];
}
