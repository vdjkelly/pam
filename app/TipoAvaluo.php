<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAvaluo extends Model
{
    protected $fillable = ['nombre'];
}
