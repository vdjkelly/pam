<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObservacionFile extends Model
{
    protected $table = "observacion_files";
    
    protected $fillable = ["avaluo_observacion_id", "observacion_file"];
}
