<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AvaluoFailed extends Notification
{
    use Queueable;

    protected $caso;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($avaluo)
    {
        $this->caso = $avaluo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'failed' => 'El caso #'. $this->caso->numero_avaluo . ' fue de vuelto por el Revisor. Por favor revisar las observaciones realizadas',
            'caso_id' => $this->caso->id,
            'numero_avaluo' => $this->caso->numero_avaluo,
            'fecha_visita' => $this->caso->fecha_visita,
            'fecha_entrega_informe' => $this->caso->fecha_entrega_informe,
        ];
    }
}
