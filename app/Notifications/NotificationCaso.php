<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;

class NotificationCaso extends Notification
{
    use Queueable;

    protected $caso;

    protected $avaluo_solicitud;

    protected $entidad;

    protected $observacion;
    protected  $cliente;
    protected $facturacion;
    protected $filesSolicitud;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso, $avaluo_solicitud, $entidad, $cliente,$facturacion, $observacion, $filesSolicitud)
    {
        $this->caso = $caso;
        $this->avaluo_solicitud = $avaluo_solicitud;
        $this->entidad = $entidad;
        $this->observacion = $observacion;
        $this->cliente = $cliente;
        $this->facturacion = $facturacion;
        $this->filesSolicitud = $filesSolicitud;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /*$notifications = DB::table('notifications')->latest('id')->first();

        $url = url('/casos/'.$this->caso->id. '/show?notification='.$notifications->id);*/
        $file_certificacion = public_path($this->avaluo_solicitud->file_certificacion);
        $file_identificacion = public_path($this->avaluo_solicitud->file_identificacion);
        $file_soporte = public_path($this->avaluo_solicitud->file_soporte);
        $orden_avaluo = public_path('archivos/A-1420.xlsx');

       

        $subject = "Tienes un nuevo caso por atender " . "#". $this->caso->numero_avaluo . " " .$this->cliente->first_name . " " . $this->cliente->last_name;

        $message = (new MailMessage)
                    ->subject($subject)
                    ->markdown('mail.casos.avaluador', ['caso' => $this->caso, 'entidad' => $this->entidad,  'facturacion' => $this->facturacion, 'cliente' => $this->cliente, 'solicitud' => $this->avaluo_solicitud, 'observacion' => $this->observacion, 'contacto' => $this->avaluo_solicitud->contacto,  'telefono_contacto' => $this->avaluo_solicitud->telefono_contacto,
                    'nombre' => $this->avaluo_solicitud->nombre,
                    'attach' => $this->filesSolicitud,
                    'correo' => $this->avaluo_solicitud->correo,
                    'direccion' => $this->avaluo_solicitud->direccion,
                    /*'notifications' => $notifications*/]);

        $message->attach($orden_avaluo);

        // foreach ($this->filesSolicitud as $file) {
        //     if($file->file_certificacion != null) {
        //         if (is_file(public_path($file->file_certificacion))) {
                   
        //            $message->attach(public_path($file->file_certificacion));

        //         }
                
                
        //     }
        //     if($file->file_identificacion != null) {
        //         if (is_file(public_path($file->file_identificacion))) {
        //         $message->attach(public_path($file->file_identificacion));
        //         }
        //     }

        //     if($file->file_soporte != null) {
        //         if (is_file(public_path($file->file_soporte))) {
        //         $message->attach(public_path($file->file_soporte));
        //         }
        //     }


        //     if($file->file_escritura != null) {
        //         if (is_file(public_path($file->file_escritura))) {

        //         $message->attach(public_path($file->file_escritura));
        //         }
        //     }
        // }
        return $message; //Send mail
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'caso_id' => $this->caso->id,
            'numero_avaluo' => $this->caso->numero_avaluo,
            'fecha_visita' => $this->caso->fecha_visita,
            'fecha_entrega_informe' => $this->caso->fecha_entrega_informe,

        ];
    }
}
