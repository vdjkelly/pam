<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotificationAnalista extends Notification
{
    use Queueable;


    protected $caso;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($avaluo)
    {
        $this->caso = $avaluo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'text' => 'El caso #'. $this->caso->numero_avaluo . ' necesita revisi&oacute;n',
            'caso_id' => $this->caso->id,
            'numero_avaluo' => $this->caso->numero_avaluo,
            'fecha_visita' => $this->caso->fecha_visita,
            'fecha_entrega_informe' => $this->caso->fecha_entrega_informe,
        ];
    }
}
