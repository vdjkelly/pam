<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotificationComment extends Notification
{
    use Queueable;

    protected $caso;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($caso)
    {
        $this->caso = $caso;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/observaciones/'.$this->caso->id. '/caso');
        return (new MailMessage)
        ->subject("Nuevo comentario #". $this->caso->id . " ". $this->caso->cliente->first_name . " ". $this->caso->cliente->first_name . "")
                    ->line('Tienes un comentario en el caso # '. $this->caso->id . " del cliente ". $this->caso->cliente->first_name . " " .  $this->caso->cliente->last_name)
                    ->action('Ir al sitio web', $url)
                    ->line('Gracias!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
