<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_name', 'username', 'telefono', 'celular', 'nit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function avaluo()
    {
        return $this->hasMany(Avaluo::class);
    }

    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }

    public function facturacion()
    {
        return $this->hasMany(Facturacion::class, 'cliente_id', 'id');
    }

    public function getRoleName(){
        if (!is_null($this->roles)) {
            return $this->roles->pluck('name')->first();
        }
    }


    public function getRoleId(){
        if (!is_null($this->roles)) {
            return $this->roles->pluck('id')->first();
        }
    }

    public function tipo_cliente()
    {
        return $this->belongsTo(TipoCliente::class);
    }

    public function entidad()
    {
        return $this->hasOne(Entidad::class);
    }
}
