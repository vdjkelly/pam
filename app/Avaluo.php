<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\DatabaseNotification;
use Carbon\Carbon;

/**
 * Class Avaluo
 * @package App
 * @var status_cliente 0 En proceso, 1 Asignado Avaluador, 2 Visita realizada, 3 Visita no realizada, 4 Reasignacion de Avaluador, 5 Proceso exitoso para facturar, 6 Devuelta por revision, 7 Correcciones para revisión, 8 Avaluo enviado al cliente, 9 Devuelto por cliente, 10 En revision
 */
class Avaluo extends Model
{
    protected $fillable = ['user_id', 'cliente_id', 'tipo_avaluo_id', 'avaluo_solicitud_id', 'avaluador_id', 'revisor_tecnico_id', 'departament_id', 'city_id', 'type_predio_id', 'facturacion_id', 'numero_avaluo', 'informe', 'fecha_entrega_informe', 'fecha_solicitud', 'fecha_subida', 'fecha_visita', 'fecha_solicitud', 'fecha_entrega_cliente', 'fecha_hora_revision', 'honorarios', 'hora', 'observaciones', 'observaciones_avaluo', 'observaciones_revisor', 'visita', 'tiempo_avaluo', 'valor_comercial', 'entidad_id', 'avaluo', 'status', 'facturado', 'fecha_entrega_avaluador', 'fecha_asignacion_revisor', 'fecha_envio', 'fecha_real_envio', 'aquien'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function tipo_avaluo()
    {
        return $this->belongsTo(TipoAvaluo::class);
    }


    public function facturacion()
    {
        return $this->belongsTo(Facturacion::class);
    }
    public function avaluo_solicitud()
    {
        return $this->belongsTo(AvaluoSolicitud::class);
    }

    public function entidad()
    {
        return $this->belongsTo(Entidad::class);
    }

    public function departament()
    {
        return $this->belongsTo(Departament::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function type_predio()
    {
        return $this->belongsTo(TypePredio::class);
    }

    public function avaluador()
    {
        return $this->belongsTo(User::class);
    }

    public function revisor()
    {
        return $this->belongsTo(User::class, 'revisor_tecnico_id', 'id');
    }

    public function avaluao_observacion()
    {
        return $this->hasMany(AvaluoObservacion::class)->orderBy('created_at', 'desc');
    }

    public function file_avaluo()
    {
        return $this->hasMany(FileAvaluo::class);
    }

    public function notification()
    {
        return $this->hasMany(DatabaseNotification::class, 'notifiable_id', 'avaluador_id');
    }
}
