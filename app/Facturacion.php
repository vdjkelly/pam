<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facturacion extends Model
{
    protected $fillable = ['factura', 'telefono_factura', 'email_factura', 'direccion', 'identificacion_factura', 'file_identificacion_factura', 'banco_consigno', 'valor_anticipo', 'file_anticipo', 'cliente_id'];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function avaluo()
    {
        return $this->hasOne(Avaluo::class);
    }
}
