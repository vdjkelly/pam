<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileAvaluo extends Model
{
    protected $fillable = ['avaluo_id', 'informe'];

    public function avaluo()
    {
        return $this->belongsTo(Avaluo::class);
    }
}
