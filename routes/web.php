<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth']);
Route::get('/', 'HomeController@welcome')->name('welcome')->middleware(['guest']);


Route::get('notifications', 'NotificationController@index')->middleware(['auth']);
Route::patch('notifications/{id}/read', 'NotificationController@markAsRead')->middleware(['auth']);
Route::post('notifications/mark-all-read', 'NotificationController@markAllRead')->middleware(['auth']);
Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss')->middleware(['auth']);


Route::get('consultas', 'AvaluoController@getConsulta')->name('consultas')->middleware(['permission:consultas', 'auth']);
Route::get('avaluos-cliente', 'AvaluoController@getAvaluosCliente')->name('getAvaluosCliente')->middleware(['auth']);

Route::get('consulta/{id}', 'AvaluoController@getAvaluoDetalles')->name('getAvaluoDetalles')->middleware(['auth']);
Route::get('entidad/GetAvaluos', 'AvaluoController@getEntidadAvaluo')->name('GetAvaluosEntidades')->middleware(['auth']);

Route::prefix('casos')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'ClienteController@index')->name('casos')->middleware('permission:casos');
        Route::get('/create', 'NuevoCasoController@create')->name('casos.create')->middleware('permission:casos.create');
        Route::post('/store', 'NuevoCasoController@store')->name('casos.store')->middleware('permission:casos.getNotNewCreate');

        Route::get('/create-inf-solicitante/{id}', 'NuevoCasoController@getNotNewCreate')->name('casos.getNotNewCreate')->middleware('permission:casos.getNotNewCreate');
        Route::post('/store-inf-solicitante', 'NuevoCasoController@getInfoSolicitante')->name('casos.getInfoSolicitante');

        //Informacion para el Avaluador rutas
        Route::get('{id}/show', 'NuevoCasoController@show')->name('casos.show');
        Route::post('gestion/store', 'GestionAvaluadoController@store')->name('gestion.store');
        Route::get('gestion/avaluos', 'GestionAvaluadoController@index')->name('gestion.index');

        //Informacion para el revisor tecnico
        Route::get('revisor/avaluos', 'RevisorTecnicoController@index')->name('revisor.index');
        Route::get('revisor/{id}/show', 'RevisorTecnicoController@show')->name('revisor.show');
        Route::get('revisor/{id}/getGestionRevisor', 'RevisorTecnicoController@getGestionRevisor')->name('revisor.getGestionRevisor');
        Route::post('revisor/store', 'RevisorTecnicoController@store')->name('revisor.store');
        Route::post('revisor/getStoreRevisor', 'RevisorTecnicoController@getStoreRevisor')->name('revisor.getStoreRevisor');

        Route::get('cliente/casos', 'ClienteController@getAvaluosCliente')->name('casos.getAvaluosCliente');
        Route::get('{id}/caso/cliente', 'AvaluoController@getAvaluoShow')->name('casos-cliente.show');
        Route::patch('{id}/devolver-caso/', 'ClienteController@update')->name('casos-cliente.update');

        Route::get('/edicion/{id}/datos-cliente/', 'ClienteController@edit')->name('datos-cliente.edit');
        Route::patch('edicion/{id}/updateDatosCliente/', 'ClienteController@updateDatosCliente')->name('updateDatosCliente');
});


Route::prefix('gestion')
    ->middleware(['auth'])
    ->group(function () {
        Route::post('avaluos', 'AvaluoController@sendAvaluo')->name('gestion.store');
        Route::post('store', 'GestionAvaluadoController@store')->name('gestion.store');
        Route::get('avaluos', 'GestionAvaluadoController@index')->name('gestion.index');
});


Route::prefix('revisor')
    ->middleware(['auth'])
    ->group(function () {
        //Informacion para el revisor tecnico
        Route::get('avaluos', 'RevisorTecnicoController@index')->name('revisor.index')->middleware('permission:revisor.index');
        Route::get('{id}/show', 'RevisorTecnicoController@show')->name('revisor.show');
        Route::get('{id}/getGestionRevisor', 'RevisorTecnicoController@getGestionRevisor')->name('revisor.RevisarAvaluo')->middleware('permission:revisor.RevisarAvaluo');
        Route::post('store', 'RevisorTecnicoController@store')->name('revisor.store');
        Route::post('getStoreRevisor', 'RevisorTecnicoController@getStoreRevisor')->name('revisor.getStoreRevisor');
});

Route::prefix('revision')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'AvaluoController@index')->name('revisiones')->middleware('permission:revisiones');
        Route::get('/{id}/show', 'AvaluoController@show')->name('revisiones.show')->middleware('permission:revisiones.show');
        

});




Route::prefix('cities')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'CityController@index')->name('cities');
        Route::get('/create', 'CityController@create')->name('cities.create');
        Route::get('/{id}/show', 'CityController@show')->name('cities.show');
});


Route::prefix('entidades')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'EntidadController@index')->name('entidades');
        Route::get('/create', 'EntidadController@create')->name('entidades.create');
        Route::post('/store', 'EntidadController@store')->name('entidades.store');
        Route::get('/{id}/edit', 'EntidadController@edit')->name('entidades.edit');
        Route::patch('/{id}/update', 'EntidadController@update')->name('entidades.update');
        Route::delete('/{id}/destroy', 'EntidadController@destroy')->name('entidades.destroy');
        Route::get('/{id}/detalle-avaluo', 'EntidadController@getAvaluoDetails')->name('entidades.getAvaluoDetails');
});


Route::prefix('predios')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'TypePredioController@index')->name('predios');
        Route::get('/create', 'TypePredioController@create')->name('predios.create');
        Route::post('/store', 'TypePredioController@store')->name('predios.store');
        Route::get('/{id}/edit', 'TypePredioController@edit')->name('predios.edit');
        Route::patch('/{id}/update', 'TypePredioController@update')->name('predios.update');
        Route::delete('/{id}/destroy', 'TypePredioController@destroy')->name('predios.destroy');
});


Route::prefix('roles')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'RoleController@index')->name('roles');
        Route::get('/create', 'RoleController@create')->name('roles.create');
        Route::post('/store', 'RoleController@store')->name('roles.store');
        Route::get('/{id}/edit', 'RoleController@edit')->name('roles.edit');
        Route::patch('/{id}/update', 'RoleController@update')->name('roles.update');
        Route::delete('/{id}/destroy', 'RoleController@destroy')->name('roles.destroy');
});


Route::prefix('tipo-avaluos')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'TipoAvaluoController@index')->name('tipos_avaluos');
        Route::get('/create', 'TipoAvaluoController@create')->name('tipos_avaluos.create');
        Route::post('/store', 'TipoAvaluoController@store')->name('tipos_avaluos.store');
        Route::get('/{id}/edit', 'TipoAvaluoController@edit')->name('tipos_avaluos.edit');
        Route::patch('/{id}/update', 'TipoAvaluoController@update')->name('tipos_avaluos.update');
        Route::delete('/{id}/destroy', 'TipoAvaluoController@destroy')->name('tipos_avaluos.destroy');
});



Route::prefix('users')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'UsuarioController@index')->name('usuarios');
        Route::get('/create', 'UsuarioController@create')->name('usuarios.create');
        Route::post('/store', 'UsuarioController@store')->name('usuarios.store');
        Route::get('/{id}/edit', 'UsuarioController@edit')->name('usuarios.edit');
        Route::patch('/{id}/update', 'UsuarioController@update')->name('usuarios.update');
        Route::delete('/{id}/destroy', 'UsuarioController@destroy')->name('usuarios.destroy');
});

Route::prefix('cuenta')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/{id}/show', 'UsuarioController@show')->name('usuarios.show');
        Route::post('/getUpdateInfo', 'UsuarioController@getUpdateInfo')->name('getUpdateInfo');
    });

Route::prefix('facturaciones')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'FacturacionController@index')->name('facturaciones');
        Route::get('{id}/show', 'FacturacionController@show')->name('facturaciones.show');
        Route::get('{id}/create', 'FacturacionController@create')->name('facturaciones.create');
        Route::post('store', 'FacturacionController@store')->name('facturaciones.store');
});


Route::prefix('send-avaluo')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'AvaluoSolicitudController@index')->name('avaluos.send');
        Route::get('{id}/create', 'AvaluoSolicitudController@create')->name('avaluos.send.create');
        Route::post('store', 'AvaluoSolicitudController@store')->name('avaluos.send.store');
});


Route::prefix('observaciones')
    ->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'ObservacionController@index')->name('observaciones.index')->middleware('permission:observaciones');
        Route::get('/{id}/caso', 'ObservacionController@show')->name('observaciones.show')->middleware('permission:observaciones');
        Route::post('store', 'ObservacionController@store')->name('observaciones.store')->middleware('permission:observaciones');
});