<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EntidadTableSeeder::class);
        $this->call(TypeClientesSeeder::class);
        $this->call(TipoAvaluoSeed::class);
        $this->call(TipoPredioTableSeeder::class);
    }
}
