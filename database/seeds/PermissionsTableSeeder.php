<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Entidades permisos
        Permission::create(['name' => 'entidad.index', 'description' => 'Lista entidades']);
        Permission::create(['name' => 'entidad.edit', 'description' => 'Editar entidad']);
        Permission::create(['name' => 'entidad.create', 'description' => 'Crear entidad']);
        Permission::create(['name' => 'entidad.update', 'description' => 'Actualizar entidad']);
        Permission::create(['name' => 'entidad.destroy', 'description' => 'Eliminar entidad']);

        //Predios permisos
        Permission::create(['name' => 'predios.index', 'description' => 'Lista predios']);
        Permission::create(['name' => 'predios.edit', 'description' => 'Editar predio']);
        Permission::create(['name' => 'predios.create', 'description' => 'Crear predio']);
        Permission::create(['name' => 'predios.destroy', 'description' => 'Eliminar predio']);

        //Roles permisos
        Permission::create(['name' => 'roles', 'description' => 'Lista de roles']);
        Permission::create(['name' => 'roles.create', 'description' => 'Crear roles']);
        Permission::create(['name' => 'roles.edit', 'description' => 'Actualizar roles']);
        Permission::create(['name' => 'roles.destroy', 'description' => 'Eliminar roles']);

        //Crear usuarios
        Permission::create(['name' => 'usuarios', 'description' => 'Lista de usuarios del sistema']);
        Permission::create(['name' => 'usuarios.create', 'description' => 'Crear usuarios del sistema']);
        Permission::create(['name' => 'usuarios.edit', 'description' => 'Actualizar usuarios del sistema']);
        Permission::create(['name' => 'usuarios.destroy', 'description' => 'Eliminar usuarios del sistema']);


        //Listado de casos
        Permission::create(['name' => 'casos', 'description' => 'Lista de casos']);
        Permission::create(['name' => 'casos.create', 'description' => 'Gestionar Datos del cliente']);
        Permission::create(['name' => 'casos.getNotNewCreate', 'description' => 'Radicar/ Asignar casos avaluador']);





        //Permisos asignacion de caso Avaluador
        Permission::create(['name' => 'casos.show', 'description' => 'Realizar visita/ Subir Avaúo al sistema']);
       // Permission::create(['name' => 'revisor.getGestionRevisor', 'description' => 'Asignar Revisor']);
        Permission::create(['name' => 'revisiones', 'description' => 'Lista de casos para revision']);
        Permission::create(['name' => 'revisiones.show', 'description' => 'Asignar Revisor']);
        Permission::create(['name' => 'revisor.index', 'description' => 'Lista de casos asignados (Revisor)']);
        Permission::create(['name' => 'revisor.RevisarAvaluo', 'description' => 'Revisar Avaluo (Revisor)']);
        Permission::create(['name' => 'facturaciones', 'description' => 'Lista de casos facturacion']);

        Permission::create(['name' => 'envios', 'description' => 'Envío de avalúo']);
        Permission::create(['name' => 'consultas', 'description' => 'Consultas de avalúos']);



        Permission::create(['name' => 'cliente.editar', 'description' => 'Edicion de casos']);


        Permission::create(['name' => 'observacion.index', 'description' => 'Listado de casos para una observacion']);
        
        Permission::create(['name' => 'observacion.crear', 'description' => 'Realizar obsevaciones a casos']);

        Role::create(['name' => 'Administrador']);


        $asesor = Role::create(['name' => 'Asesor Comercial']);
        $asesor->givePermissionTo([
            'casos',
            'casos.create',
            'consultas'
        ]);

        $analista = Role::create(['name' => 'Analista de Avaluos']);
        $analista->givePermissionTo([
            'revisiones',
            'revisiones.show',
            'casos.getNotNewCreate',
            'consultas'
        ]);


        $avaluador = Role::create(['name' => 'Avaluador']);
        $avaluador->givePermissionTo([
            'casos.show',
            'consultas'
        ]);

        $revisor = Role::create(['name' => 'Revisor Técnico']);
        $revisor->givePermissionTo([
            'revisor.RevisarAvaluo',
            'revisor.index',
            'consultas'
        ]);

        $director = Role::create(['name' => 'Director Avaluos']);

        $director->givePermissionTo([
            'facturaciones',
            'revisiones',
            'consultas'
        ]);


        $facturacion = Role::create(['name' => 'Analista de Facturacion']);
        $facturacion->givePermissionTo([
            'facturaciones',
            'consultas'
        ]);

        Role::create(['name' => 'Cliente']);
        Role::create(['name' => 'Entidad']);
    }
}
