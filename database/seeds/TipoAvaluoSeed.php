<?php

use Illuminate\Database\Seeder;

class TipoAvaluoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\TipoAvaluo::create([
            'nombre' => 'Hipotecario'
        ]);

        \App\TipoAvaluo::create([
            'nombre' => 'Mayorista'
        ]);

        \App\TipoAvaluo::create([
            'nombre' => 'Propio'
        ]);

        \App\TipoAvaluo::create([
            'nombre' => 'Externos'
        ]);
    }
}
