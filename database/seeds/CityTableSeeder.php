<?php

use Illuminate\Database\Seeder;
use App\City;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::create([
            'nombre' => 'Abriaquí',
            'departament_id' => 5
        ]);

        City::create([
            'nombre' => 'Alejandria',
            'departament_id' => 5
        ]);

        City::create([
            'nombre' => 'Amagá',
            'departament_id' => 5
        ]);


        City::create([
            'nombre' => 'Campo de la Cruz',
            'departament_id' => 8
        ]);

        City::create([
            'nombre' => 'Candelaria',
            'departament_id' => 8
        ]);

        City::create([
            'nombre' => 'Juan de Acosta',
            'departament_id' => 8
        ]);
    }
}
