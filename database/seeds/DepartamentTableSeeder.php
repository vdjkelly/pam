<?php

use Illuminate\Database\Seeder;
use App\Departament;

class DepartamentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Departament::create([
            'id' => 5,
            'nombre' => 'ANTIOQUIA'
        ]);

        Departament::create([
            'id' => 8,
            'nombre' => 'ATLÁNTICO'
        ]);

        Departament::create([
            'id' => 11,
            'nombre' => 'BOGOTÁ, D.C.'
        ]);

        Departament::create([
            'id' => 13,
            'nombre' => 'BOLÍVAR'
        ]);

        Departament::create([
            'id' => 15,
            'nombre' => 'BOYACÁ'
        ]);

        Departament::create([
            'id' => 17,
            'nombre' => 'CALDAS'
        ]);
    }
}
