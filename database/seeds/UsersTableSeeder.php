<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username' => 'admin',
            'name' => 'Jesus',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
            //'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'admin@gmail.com'
        ]);

        $user->assignRole('Administrador');


        $asesor = User::create([
            'username' => 'asesor',
            'name' => 'asesor',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
           // 'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'asesor@gmail.com'
        ]);

        $asesor->assignRole('Asesor Comercial');


        $analista = User::create([
            'username' => 'Analista',
            'name' => 'Analista',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
           // 'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'analista@gmail.com'
        ]);

        $analista->assignRole('Analista de Avaluos');


        $avaluador = User::create([
            'username' => 'avaluador',
            'name' => 'avaluador',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
          //  'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'avaluador@gmail.com'
        ]);

        $avaluador->assignRole('Avaluador');

        $director = User::create([
            'username' => 'director',
            'name' => 'director',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
          //  'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'director@gmail.com'
        ]);

        $director->assignRole('Director Avaluos');

        $revisor = User::create([
            'username' => 'revisor',
            'name' => 'revisor',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
           // 'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'revisor@gmail.com'
        ]);

        $revisor->assignRole('Revisor Técnico');


        $facturacion = User::create([
            'username' => 'facturacion',
            'name' => 'facturacion',
            'last_name' => 'Salazar',
            'telefono' => '0412524441',
            'celular' => '002939388',
          //  'type' => 'administrador',
            'password' => bcrypt('1234567'),
            'email' => 'facturacion@gmail.com'
        ]);


        $facturacion->assignRole('Analista de Facturacion');
    }
}
