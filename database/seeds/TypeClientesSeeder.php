<?php

use Illuminate\Database\Seeder;
use App\TipoCliente;

class TypeClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoCliente::create([
            'name' => 'Natural'
        ]);

        TipoCliente::create([
            'name' => 'Juridico'
        ]);
    }
}
