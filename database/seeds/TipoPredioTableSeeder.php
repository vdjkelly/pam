<?php

use Illuminate\Database\Seeder;
use App\TypePredio;

class TipoPredioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypePredio::create([
            'nombre' => 'Casa'
        ]);

        TypePredio::create([
            'nombre' => 'apartamento'
        ]);

        TypePredio::create([
            'nombre' => 'lote'
        ]);
    }
}
