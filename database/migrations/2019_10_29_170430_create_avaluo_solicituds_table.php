<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaluoSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaluo_solicituds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('certificacion')->nullable();
            $table->string('escritura')->nullable();
           // $table->string('file_certificacion')->nullable();
            $table->string('identificacion')->nullable();
           // $table->string('file_identificacion')->nullable();
            $table->string('soporte')->nullable();
            $table->string('file_soporte')->nullable();
            $table->text('direccion')->nullable();
            $table->string('consecutivo_entidades')->nullable();
            $table->string('contacto')->nullable();
            $table->string('correo')->nullable();
            $table->string('nombre')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();

            $table->string('telefono_contacto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaluo_solicituds');
    }
}
