<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileAvaluoSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_avaluo_solicituds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('avaluo_solicitud_id');
            $table->string('file_certificacion')->nullable()->default(null);
            $table->string('file_identificacion')->nullable()->default(null);
            $table->string('file_escritura')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_avaluo_solicituds');
    }
}
