<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturacions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('cliente_id');
            $table->string('factura')->nullable();
            $table->string('telefono_factura')->nullable();
            $table->string('email_factura')->nullable();
            $table->text('direccion')->nullable();
            $table->string('identificacion_factura')->nullable();
            $table->string('file_identificacion_factura')->nullable();
            $table->string('banco_consigno')->nullable();
            $table->string('valor_anticipo')->nullable();
            $table->string('file_anticipo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturacions');
    }
}
