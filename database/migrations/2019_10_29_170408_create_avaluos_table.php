<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvaluosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaluos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('cliente_id');
            $table->unsignedInteger('entidad_id')->nullable();
            $table->unsignedInteger('tipo_avaluo_id')->nullable();
            $table->unsignedInteger('avaluo_solicitud_id')->nullable();
            $table->unsignedInteger('avaluador_id')->nullable();
            $table->unsignedInteger('revisor_tecnico_id')->nullable();
            $table->unsignedInteger('departament_id')->nullable();
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('type_predio_id')->nullable();
            $table->unsignedInteger('facturacion_id')->nullable();
            $table->string('numero_avaluo')->nullable();
            $table->string('informe')->nullable();
            $table->timestamp('fecha_entrega_informe')->nullable();
            $table->timestamp('fecha_solicitud')->nullable();
            $table->timestamp('fecha_subida')->nullable();
            $table->timestamp('fecha_visita')->nullable();
            $table->timestamp('fecha_entrega_cliente')->nullable();
            $table->timestamp('fecha_hora_revision')->nullable();
            $table->string('honorarios')->nullable();
            $table->string('hora_visita')->nullable();
            $table->text('observaciones')->nullable();
            $table->text('observaciones_avaluo')->nullable();
            $table->text('observaciones_revisor')->nullable();
            $table->boolean('visita')->default(0);
            $table->boolean('avaluo')->default(0);
            $table->boolean('enviado')->default(0);
            $table->boolean('status')->default(0);
            $table->boolean('facturado')->default(0);
            $table->timestamp('tiempo_avaluo')->nullable();
            $table->string('valor_comercial')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaluos');
    }
}
